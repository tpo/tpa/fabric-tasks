#!/usr/bin/python3

"""Dev environment setup for this repository."""

__epilog__ = """This install Python requirements specified in the setup.cfg file
through Debian packages instead of pip.

It also installs git hooks so that linting and tests are done before
push.
"""

import argparse
import logging
from os import symlink
from subprocess import run, PIPE

import re
import configparser


SUDO: str | None = "sudo"
NOOP = False


def install_packages(names: list[str]):
    global SUDO, NOOP
    logging.info("ensuring %d packages are installed: %s", len(names), " ".join(names))
    install_list = []
    for name in names:
        if run(["dpkg-query", "--show", name], stdout=PIPE).returncode != 0:
            install_list.append(name)
    if install_list:
        logging.info(
            "installing %d packages: %s%s",
            len(install_list),
            " ".join(install_list),
            " (simulated)" if NOOP else "",
        )
        if NOOP:
            return
        if SUDO:
            run([SUDO, "apt", "install"] + install_list, check=True)
        else:
            run(["apt", "install"] + install_list, check=True)


def install_python_requirements():
    config = configparser.ConfigParser()
    config.read("setup.cfg")

    requirements = [
        x
        for x in re.split(
            r"[\n ;]",
            config.get("options", "install_requires")
            + config.get("options.extras_require", "test"),
        )
        if x
    ]

    install_packages(["dh-python", "python3-typeshed"])

    logging.info(
        "finding equivalent Debian packages for %d Pip packages: %s",
        len(requirements),
        " ".join(requirements),
    )
    ret = run(
        ["python3", "/usr/share/dh-python/dhpython/pydist.py"] + requirements,
        check=True,
        stdout=PIPE,
        encoding="utf-8",
    )

    deb_reqs = ret.stdout.split()
    install_packages(deb_reqs)


def install_git_hooks():
    global NOOP
    logging.info("installing pre-commit git hook%s", " (simulated)" if NOOP else "")
    try:
        symlink("../../lint.sh", ".git/hooks/pre-push")
    except FileExistsError:
        pass


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized before this, using
    `basicConfig`.
    """

    def __init__(self, *args, **kwargs):  # type: ignore[no-untyped-def]
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):  # type: ignore[no-untyped-def]
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)
        # cargo-culted from _StoreConstAction
        setattr(ns, self.dest, self.const or values)


def main() -> None:
    global SUDO, NOOP
    parser = argparse.ArgumentParser(description=__doc__, epilog=__epilog__)
    logging.basicConfig(level="INFO", format="%(message)s")
    parser.add_argument(
        "-q",
        "--quiet",
        action=LoggingAction,
        const="WARNING",
        help="silence messages except warnings and errors",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    parser.add_argument(
        "-n",
        "--nothing",
        action="store_true",
        help="do nothing permanent",
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--sudo-command",
        default=SUDO,
        help="command to use to become root, default: %(default)s",
    )
    group.add_argument(
        "--no-sudo",
        action="store_true",
        help="do not use a sudo command",
    )
    args = parser.parse_args()
    if args.no_sudo:
        SUDO = None
    else:
        SUDO = args.sudo_command
    NOOP = args.nothing

    install_python_requirements()
    install_git_hooks()


if __name__ == "__main__":
    main()
