#!/usr/bin/python3

# this is a do-nothing script to aid (and eventually automate) membership/account retirements
# https://blog.danslimmon.com/2019/07/15/do-nothing-scripting-the-key-to-gradual-automation/

import re
import sys

try:
    import mistune
except ImportError:
    print("the mistune package are required.")
    print("run `pip install mistune` and try again")
    exit(0)


__preamble = """
./membership-retirements-do-nothing.py <username> <service_wiki_page>
"""


def wait():
    input("press enter to continue...")


class NoopRunner:
    def run(self, context):
        pass


class LockAccountStep(NoopRunner):
    def run(self, context):
        context["ticket_number"] = input("enter a ticket number if relevant: ").strip()
        if context["ticket_number"]:
            print(
                'run:\n\tssh -tt db.torproject.org ud-lock -r "{}" {}'.format(
                    context["ticket_number"], context["username"]
                )
            )
        else:
            print(
                "run:\n\tssh -tt db.torproject.org ud-lock {}".format(
                    context["username"]
                )
            )
        wait()


class CheckFileOwnershipStep(NoopRunner):
    def run(self, context):
        print(
            "run:\n\tcumin -p 30 -b 5 '*' 'find / -user {} -o -group {} || true'".format(
                context["username"], context["username"]
            )
        )
        print("then change ownership of any files")
        wait()


class TableOnlyExtractor(mistune.HTMLRenderer):
    new_row = False
    last_cell = None
    current_service = None
    service_auth_map: dict[str, str | None] = {}
    skip_section = False
    heading_row_count = 0

    def heading(self, text, level, **attrs):
        if text.lower() in ("retired", "documentation assessment"):
            self.skip_section = True
        else:
            self.skip_section = False
        return text

    def table_head(self, text):
        self.heading_row_count = 1
        return text

    def table_row(self, text):
        self.new_row = True
        self.heading_row_count += 1
        return text

    def table_cell(self, text, align=None, head=False):
        if self.heading_row_count <= 1:
            return text
        if self.new_row:
            self.new_row = False
            if (
                not self.skip_section
                and self.current_service
                and self.last_cell.lower() not in ("n/a", "no", "ldap")
            ):
                self.service_auth_map[self.current_service] = self.last_cell
            self.current_service = re.sub(r"<[^>]*>", "", text)  # remove markup
        self.last_cell = re.sub(r"<[^>]*>", "", text)  # remove markup
        return text

    def wrap_up(self):
        if (
            not self.skip_section
            and self.current_service
            and not self.service_auth_map.get(self.current_service)
        ):
            self.service_auth_map[self.current_service] = self.last_cell


class RevokeServiceAccessStep(NoopRunner):
    def parse_tables(self, context):
        with open(context["service_wiki_page"]) as fp:
            content = fp.read()
        renderer = TableOnlyExtractor()
        markdown = mistune.create_markdown(renderer=renderer, plugins=["table"])
        markdown(content)
        renderer.wrap_up()
        for service, auth in renderer.service_auth_map.items():
            print(f" - [ ] {service}: {auth}")

    def run(self, context):
        print(
            "I'll parse the service tables for you. I'll show you what kind of auth a service"
            "has, and who the service maintainer is so you can contact them."
        )
        print("Fair warning, there's going to be a lot of output")
        wait()
        self.parse_tables(context)
        print(
            "go through this service list, and contact relevant service maintainers about"
            "deactivating the account for " + context["username"]
        )
        wait()


def main():
    try:
        context = {
            "username": sys.argv[1],
            "service_wiki_page": sys.argv[2],
        }
    except IndexError:
        exit(__preamble)

    procedure: list[NoopRunner] = [
        LockAccountStep(),
        CheckFileOwnershipStep(),
        RevokeServiceAccessStep(),
    ]
    for step in procedure:
        step.run(context)
    print("done")


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print("unexpected error", e)
        import pdb
        import traceback

        traceback.print_exc()
        pdb.post_mortem()
        sys.exit(1)
