#!/usr/bin/python3

import fileinput
import json
import logging


def main():
    logging.basicConfig(level="INFO")
    try:
        for line in fileinput.input(openhook=fileinput.hook_compressed):
            if fileinput.isfirstline():
                logging.info("parsing %s", fileinput.filename())
            try:
                blob = json.loads(line)
            except ValueError as e:
                logging.warning(
                    "failed to parse %s:%d: %s",
                    fileinput.filename(),
                    fileinput.lineno(),
                    e,
                )
                continue
            for param in blob.get("params", []):
                try:
                    email = param.get("value", {}).get("email")
                except AttributeError:
                    logging.debug("ignoring string value: %s", param.get("value"))
                    continue
                if email:
                    logging.info("email: %r", email)
    except OSError as e:
        logging.error(
            "error while processing file %s at line %s: %s, skipping",
            fileinput.filename(),
            fileinput.lineno(),
            e,
        )
        fileinput.nextfile()


if __name__ == "__main__":
    main()
