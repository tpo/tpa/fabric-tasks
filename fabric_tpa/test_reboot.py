from . import reboot


APT_LIST_OUTPUT = """Listing...

apache2-utils/testing,unstable 2.4.57-2 amd64 [upgradable from: 2.4.56-1]
borgbackup-doc/testing,unstable 1.2.4-1 all [upgradable from: 1.2.3-1]
borgbackup/testing,unstable 1.2.4-1 amd64 [upgradable from: 1.2.3-1+b1]
chromium-common/stable-security 112.0.5615.121-1~deb11u1 amd64 [upgradable from: 112.0.5615.49-2]"""


def test_parse_apt_list():
    assert list(reboot.parse_apt_list(APT_LIST_OUTPUT)) == [
        "apache2-utils",
        "borgbackup-doc",
        "borgbackup",
        "chromium-common",
    ]
