from .gitolite import (
    change_category_str,
    list_conf_repos_str,
    _list_conf_repos_categories_str,
    list_repos_access_str,
)


TEST_SAMPLE_GITOLITE_CONF = """
@webwml                                      = foo bar baz

# a comment

repo admin/tor-jenkins
    RW                                       = @torproject-admin
    config hooks.irc-enabled                 = true
    config hooks.ircproject                  = tor-admin
    config gitweb.category                   = Attic
admin/tor-jenkins "The Tor Project" = "Tor Project jenkins configuration"

repo admin/no-category
    RW                                       = @torproject-admin
    config hooks.irc-enabled                 = true
    config hooks.ircproject                  = tor-admin
admin/tor-jenkins "The Tor Project" = "Tor Project jenkins configuration"

repo admin/account-keyring
    RW                                       = @torproject-admin
    R                                        = @all
    config hooks.irc-enabled                 = true
    config hooks.ircproject                  = tor-admin

repo censorship-timeline
    RW                                       = @censorship-timeline
    config multimailhook.mailinglist                 = tor-commits@lists.torproject.org
    config hooks.irc-enabled                 = true
    config hooks.ircproject                  = or
censorship-timeline "The Tor Project" = "censorship-timeline for tor with code, data, and ..."

"""


TEST_SAMPLE_GITOLITE_CONF_MODIFIED = """
@webwml                                      = foo bar baz

# a comment

repo admin/tor-jenkins
    RW                                       = @torproject-admin
    config hooks.irc-enabled                 = true
    config hooks.ircproject                  = tor-admin
    config gitweb.category = Foo
admin/tor-jenkins "The Tor Project" = "Tor Project jenkins configuration"

repo admin/no-category
    RW                                       = @torproject-admin
    config hooks.irc-enabled                 = true
    config hooks.ircproject                  = tor-admin
    config gitweb.category = Foo
admin/tor-jenkins "The Tor Project" = "Tor Project jenkins configuration"

repo admin/account-keyring
    RW                                       = @torproject-admin
    R                                        = @all
    config hooks.irc-enabled                 = true
    config hooks.ircproject                  = tor-admin
    config gitweb.category = Bar

repo censorship-timeline
    RW                                       = @censorship-timeline
    config multimailhook.mailinglist                 = tor-commits@lists.torproject.org
    config hooks.irc-enabled                 = true
    config hooks.ircproject                  = or
    config gitweb.category = Baz
censorship-timeline "The Tor Project" = "censorship-timeline for tor with code, data, and ..."
"""


def test_change_category_str():
    result = list(
        change_category_str(TEST_SAMPLE_GITOLITE_CONF, "admin/tor-jenkins", "Foo")
    )
    assert "    config gitweb.category = Foo" in list(result)
    result = list(change_category_str("\n".join(result), "admin/no-category", "Foo"))
    assert "    config gitweb.category = Foo" in list(result)
    result = list(
        change_category_str("\n".join(result), "admin/account-keyring", "Bar")
    )
    assert "    config gitweb.category = Bar" in result
    result = list(change_category_str("\n".join(result), "censorship-timeline", "Baz"))
    assert "    config gitweb.category = Baz" in result
    print("\n".join(result))
    assert "\n".join(result) + "\n" == TEST_SAMPLE_GITOLITE_CONF_MODIFIED


def test_parse_conf():
    assert list(list_conf_repos_str(TEST_SAMPLE_GITOLITE_CONF)) == [
        "admin/tor-jenkins",
        "admin/no-category",
        "admin/account-keyring",
        "censorship-timeline",
    ]
    assert list(_list_conf_repos_categories_str(TEST_SAMPLE_GITOLITE_CONF)) == [
        ("admin/tor-jenkins", "Attic"),
        ("admin/no-category", None),
        ("admin/account-keyring", None),
        ("censorship-timeline", None),
    ]


def test_list_repos_access():
    assert list(list_repos_access_str(TEST_SAMPLE_GITOLITE_CONF.splitlines())) == [
        ("admin/tor-jenkins", [("RW", "@torproject-admin")]),
        ("admin/no-category", [("RW", "@torproject-admin")]),
        ("admin/account-keyring", [("RW", "@torproject-admin"), ("R", "@all")]),
        ("censorship-timeline", [("RW", "@censorship-timeline")]),
    ]
