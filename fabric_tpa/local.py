import getpass
import logging
import os
import time


"""local utilities that do not fit anywhere else

Those are functions exclusively designed to run on the local machine,
not even the local Context(): they are raw, pure Python functions that
can be reused elsewhere.

This is not in the host module to avoid dependency loops, among other
things."""


# provide a fallback for tqdm's trange if not available
try:
    from tqdm import tqdm

    def trange(stop: int):
        # we need to effectively rewrite the upstream "trange"
        # function because we want the bar to resize along window
        # changes, see https://github.com/tqdm/tqdm/issues/631
        return tqdm(range(stop), dynamic_ncols=True)

except ImportError:

    def trange(stop: int):
        return range(stop)


def getuser():
    """get the local username, defaults to getpass.getuser()

    This calls getpass.getuser() unless a TPA_USERNAME environment is
    available, to override the default."""
    return os.environ.get("TPA_USERNAME", getpass.getuser())


def sleep_with_progress(seconds: int) -> None:
    """sleep the given amount of time, hopefully with a progress bar

    This is *not* as accurate as time.sleep(), as the error of each
    time.sleep(1) call accumulates over time. The margin of error is
    one second.

    Experimentally, it seems the error is about 0.37%, in other words,
    sleeping 100 seconds actually sleeps 100.037 seconds, or 37ms too
    much.

    The one second rounding errors should then typically happen after
    waiting for 43 minutes, and will not get worse after that.

    Note that this just sleeps, without any callback. If you *do* need
    to do something while sleeping (say, every second) and still want
    that fancy progress bar, use the `trange` function provided by
    this module and manually sleep at each step, for example instead
    of:

        sleep_with_progress(10)

    do:

        for i in trange(10):
            logging.info("trying to foobar (%d/10)...", i)
            foobar()
            time.sleep(1)
    """
    try:
        from tqdm import tqdm
    except ImportError:
        return time.sleep(seconds)
    start_time = time.time()
    for _ in tqdm(range(seconds)):
        if (time.time() - start_time) > seconds:
            break
        time.sleep(1)
    logging.debug("slept %s", time.time() - start_time)
