"""Alertmanager silence API

This reimplements the `amtool` commandline tool but with support for
running commands locally.

We're not implementing things like deleting or listing silences, for
which we encourage people to use the web interface unless there's a
strong use case for automation.

This could have been implemented with remote fabric tasks that
actually run amtool instead, but then it's hard to pass those
arguments verbatim because of:

https://github.com/pyinvoke/invoke/issues/378

"""

from datetime import datetime, timedelta, timezone
import logging
import re

from invoke import Context
import invoke.exceptions
from fabric import task

try:
    import dateparser

    dateparser_available = True
except ImportError:
    dateparser_available = False


import requests
import requests.exceptions

from . import local


def parse_date(date_str: str) -> datetime:
    """parse the given date, first as an ISO timestamp, then passing it down to dateparser"""
    try:
        return datetime.fromisoformat(date_str)
    except ValueError as e:
        if not dateparser_available:
            raise invoke.exceptions.Exit(
                "failed to parse time %s as an ISO 8601 timestamp, try installing dateparser: %s"
                % (
                    date_str,
                    e,
                )
            )
    parsed_datetime = dateparser.parse(date_str, settings={"TIMEZONE": "UTC"})
    if not parsed_datetime:
        raise invoke.exceptions.Exit("failed to parse date %s, aborting" % date_str)
    return parsed_datetime


@task(iterable=["matchers"])
def create(
    con: Context,
    matchers: list[str],
    comment: str,
    created_by: str | None = None,
    starts_at: datetime | str | None = None,
    ends_at: datetime | str | None = None,
    http_user: str = "tor-guest",
    http_password: str = "",
    url: str = "https://alertmanager.torproject.org",
):
    """Create a silence in Alertmanager

    Matchers are a series of key=value strings. For example, matching
    the JobDown alert on host test.example.com would be done with:

    --matchers alertname=JobDown --matchers alias=test.example.com

    The starts_at default to "now" and ends_at default to "an hour
    from now", otherwise should be specified as RFC 3339 (AKA ISO
    8601) strings, e.g. 2024-10-16T15:10:11.484Z.

    If the dateparser Python module is available, it can be used to
    specify dates in a more natural way, for example "in 2 days".

    This is a similar interface as the silence.update command, except
    with a "None" for the silence id, which in alert manager land
    creates a new silence.
    """
    return update(
        con,
        matchers,
        comment,
        None,
        created_by,
        starts_at,
        ends_at,
        http_user,
        http_password,
        url,
    )


@task(iterable=["matchers"])
def update(
    con: Context,
    matchers: list[str],
    comment: str,
    silence_id: str | None,
    created_by: str | None = None,
    starts_at: datetime | str | None = None,
    ends_at: datetime | str | None = None,
    http_user: str = "tor-guest",
    http_password: str = "",
    url: str = "https://alertmanager.torproject.org",
):
    """Update a silence in Alertmanager

    Matchers are a series of key=value strings. For example, matching
    the JobDown alert on host test.example.com would be done with:

    --matchers alertname=JobDown --matchers alias=test.example.com

    The starts_at default to "now" and ends_at default to "an hour
    from now", otherwise should be specified as RFC 3339 (AKA ISO
    8601) strings, e.g. 2024-10-16T15:10:11.484Z.

    If the dateparser Python module is available, it can be used to
    specify dates in a more natural way, for example "in 2 days".

    If silence-id is set to a not-None value, the silence with the given
    uuid-like id will be updated instead of creating a new silence.
    """

    # set sane defaults
    if not starts_at or starts_at == "now":
        # start is "now" by default
        starts_at = datetime.now(timezone.utc)
    else:
        if not isinstance(starts_at, datetime):
            starts_at = parse_date(starts_at)
    if not ends_at:
        # ends in one hour by default
        ends_at = datetime.now(timezone.utc) + timedelta(hours=1)
    else:
        if not isinstance(ends_at, datetime):
            ends_at = parse_date(ends_at).replace(tzinfo=timezone.utc)

    if not created_by:
        # use the local username as a default for the creator
        created_by = local.getuser()

    # parse a series of key,value pairs with one of the permitted matcher
    # operators and add them to the matcher_objs object
    matcher_operators = re.compile(r"\s*(!=|=~|!~|=)\s*")
    matcher_objs = []
    error = False
    for matcher in matchers:
        parts = matcher_operators.split(matcher, maxsplit=1)
        if len(parts) != 3:
            logging.error(
                "badly specified matcher. needed `key (=|=~|!=|!~) value`: %s", matcher
            )
            error = True
            continue
        name, op, value = parts
        # defaults are for '='
        is_regex = False
        is_equal = True
        match op:
            case "=~":
                is_regex = True
            case "!=":
                is_equal = False
            case "!~":
                is_regex = True
                is_equal = False
        matcher_objs.append(
            {
                "name": name,
                "value": value,
                "isRegex": is_regex,
                "isEqual": is_equal,
            }
        )
    if error:
        raise invoke.exceptions.Exit("errors while parsing matchers, aborting")

    # post the silence
    # documentation of this API:
    # https://petstore.swagger.io/?url=https://raw.githubusercontent.com/prometheus/alertmanager/main/api/v2/openapi.yaml#/silence/postSilences
    silence_obj = {
        "matchers": matcher_objs,
        "startsAt": starts_at.isoformat(),
        "endsAt": ends_at.isoformat(),
        "createdBy": created_by,
        "comment": comment,
    }
    # id is optional, it's used to update an existing one. this
    # code could be therefore refactored to support modifying
    # existing alerts as well.
    if silence_id:
        silence_obj["id"] = silence_id
    logging.info(
        "adding silence from %s to %s (%s), created by: %s, comment: %s, matchers: %s%s",
        starts_at.isoformat(timespec="seconds"),
        ends_at.isoformat(timespec="seconds"),
        ends_at - starts_at,
        created_by,
        comment,
        ", ".join(matchers),
        " (dry run)" if con.config.run.dry else "",
    )
    if con.config.run.dry:
        return
    logging.debug("silence object: %r", silence_obj)
    result = requests.post(
        url + "/api/v2/silences", json=silence_obj, auth=(http_user, http_password)
    )

    # error handling
    logging.debug("response code: %s", result)
    try:
        response_obj = result.json()
    except requests.exceptions.JSONDecodeError as e:
        raise invoke.exceptions.Exit(
            "cannot parse JSON response from Alertmanager: %s" % e
        )
    if result.status_code != requests.codes.ok:
        raise invoke.exceptions.Exit(
            "failed to post silence to Alertmanager, responded with status code: %s, JSON: %s"
            % (
                result.status_code,
                response_obj,
            )
        )

    # show confirmation to the user
    logging.debug("response object: %s", response_obj)
    silence_id = response_obj.get("silenceID")
    if not silence_id:
        raise invoke.exceptions.Exit(
            "silence posted, but missing silenceID from Alertmanager, status code: %s, response object: %s"
            % (result.status_code, response_obj)
        )
    logging.info("posted silence %s: %s/#/silences/%s", silence_id, url, silence_id)
