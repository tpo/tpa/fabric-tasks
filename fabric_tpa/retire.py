#!/usr/bin/python3
# coding: utf-8

"""retirement procedures"""
# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import logging
import sys


try:
    from fabric import task
except ImportError:
    sys.stderr.write(
        "cannot find fabric, install with `apt install python3-fabric`"
    )  # noqa: E501
    raise
from invoke import Context
import invoke.exceptions

from . import host, ganeti, silence


# see also https://help.torproject.org/tsa/howto/retire-a-host/


@task
def remove_backups(
    instance_con,
    retirement_delay_days: int = host.DEFAULT_RETIREMENT_DELAY_BACKUPS,
    backup_host="bungei.torproject.org",
    backup_director_host="bacula-director-01.torproject.org",
):
    """delete instance backups from the bacula storage host"""
    for backup_dir in (
        "/srv/backups/bacula/%s/" % instance_con.host,
        "/srv/backups/pg/%s/" % instance_con.host.split(".")[0],
    ):
        backup_con = host.find_context(backup_host, config=instance_con.config)
        if host.path_exists(backup_con, backup_dir):
            host.schedule_delete(
                backup_con, backup_dir, "%s days" % retirement_delay_days
            )

    con = host.find_context(backup_director_host, config=instance_con.config)
    command = "echo delete client=%s-fd yes | bconsole" % instance_con.host
    host.schedule_job(con, command, "%s days" % retirement_delay_days)


@task
def revoke_puppet(instance_con, puppetmaster="root@puppet.torproject.org"):
    """revoke certificates of given instance on puppet master"""
    con = host.find_context(puppetmaster, config=instance_con.config)
    # XXX: error handling?
    con.run("puppet node clean %s" % instance_con.host)
    con.run("puppet node deactivate %s" % instance_con.host)
    con.run("service apache2 restart")  # reload the CRL
    con.run(
        f"rm /var/lib/prometheus/node-exporter/{instance_con.host}.prom /var/lib/prometheus/node-exporter/.{instance_con.host}.prom.yaml",  # noqa: E501
        warn=True,
    )


@task
def retire_all(
    instance_con,
    parent_host=None,
    retirement_delay_vm: int = host.DEFAULT_RETIREMENT_DELAY_VM,
    retirement_delay_backups: int = host.DEFAULT_RETIREMENT_DELAY_BACKUPS,
    backup_host="bungei.torproject.org",
    backup_director_host="bacula-director-01.torproject.org",
    puppet_host="root@puppet.torproject.org",
):
    """retire an instance from its parent, backups and puppet"""
    silence.create(
        Context(),
        [
            f"alias={instance_con.host}",
        ],
        "silencing all alerts on host to be retired",
        ends_at="in 2 days",
    )
    silence.create(
        Context(),
        [
            f"alias={instance_con.host}",
            "alertname=BackupStalled",
        ],
        "silencing stalled backup issues related to retired host",
        ends_at=f"in {retirement_delay_backups} days",
    )
    silence.create(
        Context(),
        [
            f"host={instance_con.host}",
            "alertname=PuppetCatalogStale",
        ],
        "silencing stale catalog issues related to retired host",
        # 7 days is the time it takes for the `puppet_reporter` shim
        # on the Puppet server to expire old records so they stop
        # bugging us in Prometheus.
        ends_at="in 7 days",
    )
    # STEP 1, 3, 4, 5
    if parent_host:
        try:
            ganeti.retire(
                instance_con,
                retirement_delay_days=retirement_delay_vm,
                master_host=parent_host,
            )
        except invoke.exceptions.Failure as e:
            logging.error(
                "failed to retire instance %s on host %s: %s",
                instance_con.host,
                parent_host,
                e,
            )
            return 1
    else:
        logging.warning(
            "not wiping instance %s data: no parent host", instance_con.host
        )
        # TODO: destroy actual on-disk data
    # STEP 13
    if backup_host:
        logging.info(
            "scheduling %s backup disks removal on host %s and director %s",
            instance_con.host,
            backup_host,
            backup_director_host,
        )
        try:
            remove_backups(
                instance_con,
                retirement_delay_days=retirement_delay_backups,
                backup_host=backup_host,
                backup_director_host=backup_director_host,
            )
        except invoke.exceptions.Failure as e:
            logging.error(
                "failed to remove %s backups on host %s and director %s: %s",
                instance_con.host,
                backup_host,
                backup_director_host,
                e,
            )
            return 2
    # STEP 8
    if puppet_host:
        try:
            revoke_puppet(instance_con, puppet_host)
        except invoke.exceptions.Failure as e:
            logging.error(
                "failed to revoke instance %s on host %s: %s",
                puppet_host,
                instance_con.host,
                e,
            )
            return 3
    # missing:
    # STEP 6: LDAP
    # STEP 7: DNS
    # STEP 9: Puppet source
    # STEP 10: tor-passwords
    # STEP 11: let's encrypt
    # STEP 12: DNSWL
    # STEP 14: docs
    # STEP 15: upstream decommissioning
