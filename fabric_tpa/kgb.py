#!/usr/bin/python3
# coding: utf-8

"""minimalist KGB python client"""
# Copyright (C) 2020 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import hashlib
import json
import logging
import netrc
import os
from urllib.parse import urlparse
import sys
from typing import Optional
from collections.abc import Callable
from fabric.tasks import Connection

import requests

from .ui import hash_digest_hex


try:
    from fabric import task
except ImportError:
    sys.stderr.write(
        "cannot find fabric, install with `apt install python3-fabric`"
    )  # noqa: E501
    raise


class IrcNotifier:
    def __init__(self, url: str = "https://kgb-bot.torproject.org") -> None:
        """configure a IRC notifier

        The URL should *not* have a trailing slash, a /json-rpc string
        will be appended."""

        self.url = url
        self.session = requests.Session()
        self.repo_id = os.environ.get("HTTP_USER", "")
        self.password = os.environ.get("HTTP_PASSWORD", "")
        if not self.repo_id:
            try:
                netrc_file = netrc.netrc()
            except (OSError, netrc.NetrcParseError) as e:
                logging.info(
                    "not sending IRC notification: cannot parse .netrc file: %s", e
                )
            else:
                hostname = urlparse(url).netloc
                account = netrc_file.authenticators(hostname)
                if account is None:
                    logging.info(
                        "could not find IRC notification credentials in .netrc"
                    )
                else:
                    self.repo_id, _, self.password = account
        if not self.repo_id:
            raise ValueError(
                "repo_id is empty, provide it through the HTTP_USER environment to send IRC notifications"
            )
        if not self.password:
            raise ValueError(
                "password is empty, provide it through the HTTP_PASSWORD environment to send IRC notifications"
            )

    def relay_message(
        self, message: str, callback: Callable[..., None] = logging.info
    ) -> bool:
        """relay an arbitrary message to a KGB bot URL

        also immediately relays the message with the provided
        callback.
        """
        # show the operator the message we're sending as well, as is
        #
        # this is just a shortcut: we were always doing:
        #
        # logging.info(msg); relay_message(msg)
        #
        # so this feels slimpler
        callback(message)
        # most of the stuff here is from the kgb-protocol(7) manpage and
        # some static analysis of the App/KGB/Client/ServerRef.pm source
        # code, particularly the send_changes_json function
        payload = json.dumps(
            {
                "method": "relay_message",
                "params": [message],
                "version": "1.1",
                # arbitrarily picked, but should vary if we expect async
                # responses to match, which is not currently the case
                "id": 1,
            }
        )
        # from the kgb-protocol(7) manpage: the auth header is the
        # password, repo id and payload concatenated...
        to_hash = self.password + self.repo_id + payload
        # ... and SHA-1 hashed in hex (no : delimiter)
        header_auth = hash_digest_hex(
            to_hash.encode("utf-8"), hash=hashlib.sha1, sep=b":"
        ).replace(b":", b"")
        # setup the JSON-RPC headers along with the auth header and the repo id
        headers = {
            "content-type": "application/json",
            "X-KGB-Auth": header_auth.decode("ascii"),
            "X-KGB-Project": self.repo_id,
        }
        logging.debug("sending request: %s, headers: %s", payload, headers)
        # deliver the payload to the host
        try:
            response = self.session.post(
                self.url + "/json-rpc",
                data=payload,
                headers=headers,
                # timeout on connect after 1 second, read after 3 seconds
                #
                # the rationale here is that we might be rebooting the
                # bot, and we want to exit earlier... without this
                # timeout, urllib3 *eventually* times out, but it
                # takes a loong time.
                timeout=(1, 3),
            )
        except IOError as e:
            logging.warning("failed to deliver IRC notification: %s", e)
            return False
        success = response.ok and response.json()["result"] == "OK"
        try:
            response_data = response.json()
        except requests.exceptions.JSONDecodeError:
            response_data = {}
        logging.debug("response: %s, %s, success: %s", response, response_data, success)
        if not success:
            logging.warning(
                "could not relay message to KGB: %s",
                response_data.get("error", "could not parse response"),
            )
        return success


@task
def relay(_: Connection, message: str) -> None:
    try:
        kgb_client = IrcNotifier()
    except ValueError as e:
        sys.exit("no IRC notifications: %s" % e)

    kgb_client.relay_message(message)


# singleton IrcNotifier client
kgb_client: Optional[IrcNotifier] = None
kgb_client_missing_known = False


def relay_message(message: str, callback: Callable[..., None] = logging.info) -> bool:
    """wrapper around IrcNotifier.relay_message to use a singleton

    This makes it easier to handle configuration errors and managing
    the delicate callback logic.
    """
    global kgb_client, kgb_client_missing_known
    try:
        if kgb_client is None:
            kgb_client = IrcNotifier()
        return kgb_client.relay_message(message, callback=callback)
    except ValueError as e:
        callback(message)
        if kgb_client is None and not kgb_client_missing_known:
            print("no IRC notifications: %s" % e, file=sys.stderr)
            kgb_client_missing_known = True

        return False


if __name__ == "__main__":
    logging.basicConfig(format="%(message)s", level="DEBUG")
    client = IrcNotifier()
    client.relay_message("anarcat test")
