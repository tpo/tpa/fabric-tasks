import json

from fabric_tpa.prometheus import _parse_instant_results, _parse_range_results


# we want to turn this:
JSON_SAMPLE_INSTANT = """[
  {
    "metric": {
      "__name__": "up",
      "alias": "web-fsn-02.torproject.org",
      "classes": "role::undefined",
      "instance": "web-fsn-02.torproject.org:9117",
      "job": "apache",
      "team": "TPA"
    },
    "value": [
      1740593430.781,
      "1"
    ]
  },
  {
    "metric": {
      "__name__": "up",
      "alias": "web-fsn-02.torproject.org",
      "hoster": "hetzner-dc13",
      "instance": "localhost:9115",
      "job": "blackbox_ssh_banner",
      "target": "web-fsn-02.torproject.org:22",
      "team": "TPA"
    },
    "value": [
      1740593430.781,
      "1"
    ]
  }
]
"""

# into this:
"""
    input_series:
      - series: 'up{"alias"="web-fsn-02.torproject.org", "classes"="role::undefined","instance"="web-fsn-02.torproject.org:9117","job"="apache","team"="TPA"}'
        values: 1
      - series: 'up{"alias"="web-fsn-02.torproject.org", "classes"="role::undefined","instance"="web-fsn-02.torproject.org:22","job"="apache","team"="TPA"}'
        values: 1
"""  # noqa: E501


def test_instant_parse_results():
    res = list(_parse_instant_results(json.loads(JSON_SAMPLE_INSTANT)))
    assert res
    assert res == [
        (
            'up{alias="web-fsn-02.torproject.org",classes="role::undefined",instance="web-fsn-02.torproject.org:9117",job="apache",team="TPA"}',  # noqa: E501
            "1",
        ),
        (
            'up{alias="web-fsn-02.torproject.org",hoster="hetzner-dc13",instance="localhost:9115",job="blackbox_ssh_banner",target="web-fsn-02.torproject.org:22",team="TPA"}',  # noqa: E501
            "1",
        ),
    ]


JSON_SAMPLE_RANGE = """{
  "status": "success",
  "data": {
    "resultType": "matrix",
    "result": [
      {
        "metric": {
          "__name__": "up",
          "alias": "idle-fsn-01.torproject.org",
          "classes": "role::idle",
          "instance": "idle-fsn-01.torproject.org:9100",
          "job": "node",
          "team": "TPA"
        },
        "values": [
          [
            1740600000,
            "1"
          ],
          [
            1740600015,
            "1"
          ],
          [
            1740600030,
            "1"
          ]
        ]
      }
    ]
  }
}"""


def test_range_parse_results():
    res = list(_parse_range_results(json.loads(JSON_SAMPLE_RANGE)["data"]["result"]))
    assert res
    assert res == [
        (
            'up{alias="idle-fsn-01.torproject.org",classes="role::idle",instance="idle-fsn-01.torproject.org:9100",job="node",team="TPA"}',  # noqa: E501
            1740600000,
            "1",
        ),
        (
            'up{alias="idle-fsn-01.torproject.org",classes="role::idle",instance="idle-fsn-01.torproject.org:9100",job="node",team="TPA"}',  # noqa: E501
            1740600015,
            "1",
        ),
        (
            'up{alias="idle-fsn-01.torproject.org",classes="role::idle",instance="idle-fsn-01.torproject.org:9100",job="node",team="TPA"}',  # noqa: E501
            1740600030,
            "1",
        ),
    ]
