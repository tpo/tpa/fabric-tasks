"""Mailman 3 tasks"""

import logging
import os.path
import sys

try:
    from fabric import task, Connection
except ImportError:
    sys.stderr.write(
        "cannot find fabric, install with `apt install python3-fabric`"
    )  # noqa: E501
    raise
from invoke import Context

from .git import commit_and_push
from .host import ensure_line
from .ui import confirm_with_timeout, input_bell


@task
def migrate_mm2_mm3(
    con: Context,
    listname: str,
    list_domain: str = "lists.torproject.org",
    mm2_server: str = "eugeni.torproject.org",
    mm3_server: str = "lists-01.torproject.org",
    tor_puppet: str = "~/src/tor/tor-puppet/",
    transport_eugeni: str = "modules/postfix/files/transport.eugeni",
):
    """migrate a mailman 2 list to mailman 3

    This assumes a *lot* of things. First, that there's a magic script
    that can be called with `withlist` to cleanup the list before
    shipping. This script should be available in the TPA wiki, in
    service/lists but, really, should be provided by upstream MM2...

    There's a similar hack to enable DMARC mitigations on list
    creation on the MM3 server, deployed through Puppet. See
    https://gitlab.com/mailman/mailman/-/issues/1181.

    It also assumes the mailman3 server is already installed, and has
    a copy of the entirety of the mailman2 server in /var/lib/mailman.

    It also assumes a transport map is available for modification in
    Puppet.

    This should probably be moved to its own Python module, and hasn't
    been tested yet.

    """
    mm3_listname = f"{listname}@{list_domain}"
    mm2_server_con = Connection(mm2_server)
    mm3_server_con = Connection(mm3_server)
    logging.info(
        "migrating list %s from Mailman 2 server %s to Mailman 3 server %s",
        listname,
        mm2_server,
        mm3_server,
    )
    logging.info("flushing digests...")
    mm2_server_con.run(
        f"cd /var/lib/mailman/bin && sudo -u list /var/lib/mailman/bin/withlist -l -r tpa.mm2_mm3_migration_cleanup.flush_digest_mbox {listname}"  # noqa: E501
    )
    logging.info("checking for pending request...")
    mm2_server_con.run(
        f"cd /var/lib/mailman/bin && sudo -u list /var/lib/mailman/bin/withlist -l -r tpa.mm2_mm3_migration_cleanup.check_pending_reqs {listname}"  # noqa: E501
    )
    input_bell(
        "check the above, warn the owner if it says 'has ... pending requests', then press enter to actually migrate the list: "  # noqa: E501
    )

    logging.info("disabling mm2 list...")
    tor_puppet_realpath = os.path.expanduser(tor_puppet)
    transport_eugeni_path = os.path.join(
        tor_puppet_realpath,
        transport_eugeni,
    )

    for alias in (
        "",
        "-admin",
        "-owner",
        "-join",
        "-leave",
        "-subscribe",
        "-unsubscribe",
        "-request",
        "-bounces",
        "-confirm",
    ):
        list_email = f"{listname}{alias}@{list_domain}"
        ensure_line(
            con,
            transport_eugeni_path,
            f"{list_email} error:list being migrated to mailman3".encode("utf-8"),
            f"^{list_email}.*".encode("utf-8"),
        )

    commit_and_push(
        tor_puppet_realpath,
        "modules/postfix/files/transport.eugeni",
        f"disable mailing list {listname} while we migrate (tpo/tpa/team#40471",
    )

    logging.info("running puppet on mm2 server...")
    while (
        return_code := mm2_server_con.run(
            "puppet agent --test --detailed-exitcodes", warn=True
        ).return_code
    ) == 1:
        confirm_with_timeout(
            f"sleeping 15 seconds before disabling {listname}, press enter to skip delay: ",
            15,
        )
    if return_code not in (0, 2):
        logging.warning("Puppet failed with status code %d", return_code)

    logging.info("resyncing list data...")
    mm3_server_con.run(
        f"rsync --info=progress2 -a root@eugeni.torproject.org:/var/lib/mailman/lists/{listname}/ /var/lib/mailman/lists/{listname}/"  # noqa: E501
    )
    mm3_server_con.run(
        f"rsync --info=progress2 -a root@eugeni.torproject.org:/var/lib/mailman/archives/private/{listname}.mbox/ /var/lib/mailman/archives/private/{listname}.mbox/"  # noqa: E501
    )

    logging.info("creating mailing list on mailman 3 server...")
    mm3_server_con.run(f"mailman-wrapper create {mm3_listname}")

    logging.info("fixing the list DMARC munging")
    # this shouldn't be necessary, see https://gitlab.com/mailman/mailman/-/issues/1181
    mm3_server_con.run(
        f"mailman-wrapper shell -l {mm3_listname} -r tpa.mm3_tweaks.mitigate_dmarc"
    )

    logging.info("converting configuration from Mailman 2...")
    mm3_server_con.run(
        f"mailman-wrapper import21 {mm3_listname} /var/lib/mailman/lists/{listname}/config.pck"
    )

    logging.info("converting archives from Mailman 2...")
    res = mm3_server_con.run(
        f"sudo -u www-data /usr/share/mailman3-web/manage.py hyperkitty_import -l {mm3_listname} /var/lib/mailman/archives/private/{listname}.mbox/{listname}.mbox",  # noqa: E501
        warn=True,  # could fail on an empty list
    )
    if res.failed:
        input_bell("WARNING: archive conversion failed, press enter to continue: ")
    else:
        logging.info("queuing search index rebuild for the new MM3 list...")
        mm3_server_con.run(
            f"echo sudo -u www-data /usr/share/mailman3-web/manage.py update_index_one_list {mm3_listname} | batch",
            echo=True,
        )

    logging.info("forwarding list to lists-01...")
    for alias in (
        "",
        "-admin",
        "-owner",
        "-join",
        "-leave",
        "-subscribe",
        "-unsubscribe",
        "-request",
        "-bounces",
        "-confirm",
    ):
        list_email = f"{listname}{alias}@{list_domain}"
        ensure_line(
            con,
            transport_eugeni_path,
            f"{list_email} smtp:lists-01.torproject.org".encode("utf-8"),
            f"^{list_email}.*".encode("utf-8"),
        )
    commit_and_push(
        tor_puppet_realpath,
        "modules/postfix/files/transport.eugeni",
        f"redirect mailing list {listname} migrated to mailman 3 (tpo/tpa/team#40471)",
    )
    logging.info("running puppet on mm2 server...")
    while (
        return_code := mm2_server_con.run(
            "puppet agent --test --detailed-exitcodes", warn=True
        ).return_code
    ) == 1:
        confirm_with_timeout(
            f"sleeping 15 seconds before re-enabling {listname}, press enter to skip delay, or control-c if another Puppet run is coming: ",  # noqa: E501
            15,
        )
    if return_code not in (0, 2):
        logging.warning("Puppet failed with status code %d", return_code)

    logging.info("done migrating list %s!", listname)
