"""mandos module

This module encapsulate commands and things done to the mandos server

Ideally, it would allow us to bootstrap hosts on the mandos server as
well, but this might also be done in Puppet.

See https://gitlab.torproject.org/tpo/tpa/team/-/issues/40096g
"""

import json
import logging

from fabric import Connection, task

from . import host


@task
def enable(con: Connection, mandos_host: str = "mandos-01.torproject.org") -> bool:
    """enable the given host on the given mandos server

    TODO: automatically guess the mandos host from LDAP or something"""
    mandos_con = host.find_context(mandos_host, config=con.config)
    ret = mandos_con.run("mandos-ctl --enable " + con.host, warn=True, hide=True)
    # disown is not passed to run, so ret can't be None
    assert ret is not None

    if ret.ok:
        logging.info("enabled host %s on Mandos server %s", con.host, mandos_con.host)
    return ret.ok


@task(autoprint=True)
def dump_host(con: Connection, mandos_host: str = "mandos-01.torproject.org") -> dict:
    """dump the Mandos configuration of the given host as JSON"""
    mandos_con = host.find_context(mandos_host, config=con.config)
    cmd = "mandos-ctl --dump-json " + con.host
    logging.debug(cmd)
    ret = mandos_con.run(cmd, warn=True, hide=True)
    # disown is not passed to run, so ret can't be None
    assert ret is not None

    if ret.failed:
        logging.info(
            "host %s not configured on Mandos server %s", con.host, mandos_con.host
        )
        return {}
    if con.config.run.dry:
        return {}
    mandos_data = json.loads(ret.stdout)
    logging.debug("got this data: %s", mandos_data)
    return mandos_data


@task(autoprint=True)
def enabled(con: Connection, mandos_host: str = "mandos-01.torproject.org") -> bool:
    """check if the given host is configured and enabled on the given mandos server"""
    mandos_data = dump_host(con, mandos_host)
    if not mandos_data:
        return False
    return mandos_data[con.host]["Enabled"]


@task(autoprint=True)
def disabled(con: Connection, mandos_host: str = "mandos-01.torproject.org") -> bool:
    """check if the given host is configured and disabled on the given mandos server

    This is not the same as just the reverse of enabled, which can be
    False if the host is not configured.
    """
    mandos_data = dump_host(con, mandos_host)
    if not mandos_data:
        return False
    return not mandos_data[con.host]["Enabled"]
