#!/usr/bin/python3
# coding: utf-8

"""fleet-wide fabric tasks"""

from datetime import datetime, timedelta, timezone
import logging

from invoke.context import Context
from fabric import task, ThreadingGroup
import fabric.exceptions
import invoke.exceptions

from .ganeti import _list_instances, getmaster
from .host import (
    AptParsingError,
    _handle_pending_upgrades,
    find_context,
    needs_reboot,
    parse_needrestart,
)
from .prometheus import hosts_from_query
from .reboot import (
    DEFAULT_DELAY_DOWN_SECONDS,
    DEFAULT_DELAY_UP_SECONDS,
    DEFAULT_DELAY_SHUTDOWN_MINUTES,
    DEFAULT_DELAY_HOSTS_SECONDS,
    DEFAULT_REASON,
    shutdown_and_wait,
    shutdown,
    ShutdownType,
)
from .ui import confirm_with_timeout


ALL_HOSTS_QUERY = 'up{job="node",alias!="dal-rescue-02.torproject.org"}==1'


@task
def pending_upgrades(
    _: Context,
    query="apt_upgrades_pending>0",
    http_user="tor-guest",
    http_password="",
):
    """inspect pending upgrades on *all* hosts that have them, according to Prometheus.

    Use --query='ALERTS{alertname="PackagesPendingTooLong",alertstate="firing"}' to
    restrict to only hosts alerting on this.

    Note that the output of this *might* differ from the
    `apt_upgrades_pending` metric, as the node exporter collector
    (apt_info.py) has various heuristics to exclude certain packages
    from being counted and, inversely, our parser doesn't take into
    account things like pinned packages.

    """
    # equivalent shell command:
    # curl -sSL --data-urlencode query='apt_upgrades_pending>0' \
    # 'https://$HTTP_USER@prometheus.torproject.org/api/v1/query \
    # | jq -r .data.result[].metric.alias \
    # | grep -v '^null$' | paste -sd,
    all_hosts = hosts_from_query(
        query,
        http_user,
        http_password,
        sample_query=ALL_HOSTS_QUERY,
    )
    if not all_hosts:
        return
    logging.info("loading package lists from hosts")
    try:
        results = ThreadingGroup(*all_hosts).run(
            "apt-cache policy ~i", warn=True, hide=True
        )
    except fabric.exceptions.GroupException as e:
        raise invoke.exceptions.Exit(
            "one of the threads failed in group, aborting: %s" % e
        )
    all_outdated = set()
    failed_hosts = set()
    outdated_hosts = set()
    for host_con, result in results.items():
        try:
            outdated = _handle_pending_upgrades(host_con, result)
        except AptParsingError as e:
            logging.warning(
                "host %s reported an error trying to parse package lists: %s",
                host_con.host,
                e,
            )
            failed_hosts.add(host_con.host)
            continue
        if outdated:
            all_outdated.update(outdated)
            outdated_hosts.add(host_con.host)

    logging.info(
        "outdated packages (%d): %s",
        len(all_outdated),
        " ".join(sorted(map(str, all_outdated))),
    )
    if failed_hosts:
        logging.info(
            "failed hosts (%d): %s", len(failed_hosts), " ".join(sorted(failed_hosts))
        )
    if outdated_hosts:
        logging.info(
            "outdated hosts (%d): %s",
            len(outdated_hosts),
            " ".join(sorted(outdated_hosts)),
        )


PENDING_RESTARTS_QUERY = "needrestart_processes_with_outdated_libraries > 0"


@task
def pending_restarts(
    _: Context,
    query=PENDING_RESTARTS_QUERY,
    http_user="tor-guest",
    http_password="",
    limit: int = 0,
):
    """show all pending restarts according to needrestart

    This runs needrestart everywhere relevant, according to the given
    query, which by default should match the OutdatedLibraries alert.

    Results are collected and a list of affected hosts is shown, along
    with the list of services to be restarted across the fleet.
    """
    all_hosts = hosts_from_query(
        query,
        http_user,
        http_password,
        sample_query=ALL_HOSTS_QUERY,
        limit=limit,
    )
    if not all_hosts:
        return
    logging.info("loading needrestart status from %d hosts", len(all_hosts))
    try:
        results = ThreadingGroup(*all_hosts).run("needrestart -b", warn=True, hide=True)
    except fabric.exceptions.GroupException as e:
        raise invoke.exceptions.Exit(
            "one of the threads failed in group, aborting: %s" % e
        )
    all_services = set()
    all_hosts_with_services = set()
    for host_con, result in results.items():
        assert result is not None
        logging.debug("received result from host %s: %r", host_con.host, result)
        services = parse_needrestart(result.stdout, all_services=True)
        if services:
            logging.info(
                "reboot required on host %s: %s", host_con.host, " ".join(services)
            )
        else:
            logging.debug("no reboot required on host %s", host_con.host)
            continue
        all_services.update(services)
        all_hosts_with_services.add(host_con.host)
    if all_hosts_with_services:
        logging.info(
            "hosts requiring a restart (%s): %s",
            len(all_hosts_with_services),
            " ".join(sorted(all_hosts_with_services)),
        )
        logging.info(
            "services to restart (%d): %s",
            len(all_services),
            " ".join(sorted(all_services)),
        )
    else:
        logging.info("no host found requiring a restart")


PENDING_REBOOTS_QUERY = "(node_reboot_required > 0) or (needrestart_kernel_status < 1) or (needrestart_ucode_status < 1)"  # noqa: E501


@task
def pending_reboots(
    con: Context,
    query=PENDING_REBOOTS_QUERY,
    http_user="tor-guest",
    http_password="",
    limit: int = 0,
):
    """inspect pending reboots on *all* hosts that have them, according to Prometheus.

    Note that the output of this *might* differ from the actual
    metrics as the needrestart timer is asynchronous, see
    tpo/tpa/prometheus-alerts#20 for details.
    """
    return pending_restarts(con, query, http_user, http_password, limit)


@task
def reboot_fleet(
    con: Context,
    http_user="tor-guest",
    http_password="",
):
    """(currently) do-nothing task to evaluate reboots needing to be performed"""
    reboots = hosts_from_query(
        PENDING_REBOOTS_QUERY,
        http_user,
        http_password,
    )
    logging.info("found %d hosts needing a reboot", len(reboots))
    needrestart = hosts_from_query(
        PENDING_RESTARTS_QUERY,
        http_user,
        http_password,
    )
    logging.info("found %d hosts needing a restart", len(needrestart))
    all_hosts = hosts_from_query(
        ALL_HOSTS_QUERY,
        http_user,
        http_password,
    )
    logging.info("found %s hosts up", len(all_hosts))
    assert len(all_hosts)
    if len(reboots) < 10:
        logging.info(
            "less than 10 reboots to perform, likely best to reboot those by hand: fab -H %s fleet.reboot-host",
            ",".join(reboots),
        )
        logging.info("ignoring %s needrestart hosts", len(needrestart))
        return
    if max(len(reboots), len(needrestart)) / len(all_hosts) < 0.8:
        if len(reboots) > len(needrestart):
            logging.info(
                "less than 80% of the fleet needs a reboot, advising invidual reboots"
                "consider waiting for upgrade propagation"
            )
            logging.info(
                "to reboot all of those anyways, try: fab -H %s fleet.reboot-host",
                ",".join(reboots),
            )
            logging.info("ignoring %s needrestart hosts", len(needrestart))
            return
        else:
            logging.info(
                "mostly restarts (%d vs %d reboots) needed, not sure what to do",
                len(needrestart),
                len(reboots),
            )
            return
    logging.info(
        "more than 80% of the fleet requires a reboot or a restart, you should reboot everything"
    )
    print(
        """considering rebooting one of the 'idle' and rescue hosts as a test:

    fab -H idle-fsn-01.torproject.org,dal-rescue-01.torproject.org fleet.reboot-host

run those commands in parallel, paying close attention to the host list on each reboot:

"""
    )

    # XXX: hardcoded cluster list
    for cluster in ("dal", "fsn"):
        hosts = [x for x in all_hosts if cluster + "-node-" in x]
        print("gnt-%s cluster:" % cluster)
        print()
        print("    fab -H %s fleet.reboot-host --no-ganeti-migrate" % ",".join(hosts))

    print(
        """
You want to avoid rebooting mirrors at once. Ideally this script here
would handle this for you, but it doesn't right now.

Note that the above assumes only two clusters are present.

then list all remaining physical hosts:

    ssh puppetdb-01.torproject.org \\
        "curl -s -G http://localhost:8080/pdb/query/v4 --data-urlencode \\
        'query=inventory[certname] { facts.virtual = \\"physical\\" }'" \\
        | jq -r '.[].certname' | grep -v -- -node- | sort

and list all VMs outside the ganeti clusters:

    ssh db.torproject.org \\
        "ldapsearch -H ldap://db.torproject.org -x -ZZ -b 'ou=hosts,dc=torproject,dc=org' \\
        '(|(physicalHost=hetzner-cloud)(physicalHost=safespring))' hostname \\
        | grep ^hostname | sed 's/hostname: //'"

Pass each host to: fab -H HOST fleet.reboot-host. Those hosts may likely be rebooted in parallel.

"""
    )


# if this is the reboot in a batch, used to wait between hosts, used
# only if --delay-hosts-seconds is non-zero and multiple hosts are
# passed to reboot_host
first_reboot = False


@task(
    help={
        "force": "force reboot even if not needed",
        "ganeti-checks": "check for a ganeti master, can be disabled to forcibly reboot Ganeti hosts without checking",
        "ganeti-migrate": "migrate VMs before rebooting Ganeti nodes, disable to reboot VMs as we",
        "ganeti-migrate-back": "migrate VMs back after reboot, disable to leave node empty after shutdown",
        "delay-down-seconds": "how long to wait for host to shutdown, defaults to %ss"
        % DEFAULT_DELAY_DOWN_SECONDS,
        "delay-up-seconds": "how long to wait for host to come back up, default: %ss"
        % DEFAULT_DELAY_UP_SECONDS,
        "delay-shutdown-minutes": "delay passed to the shutdown command, in minutes, default %sm"
        % DEFAULT_DELAY_SHUTDOWN_MINUTES,
        "delay-hosts-seconds": "how long to wait between hosts when rebooting multiple, default %ss"
        % DEFAULT_DELAY_HOSTS_SECONDS,
        "silence-ends-at": "when the silence should end, default the sum of delay-shutdown and delay-up, except for halt, where the silence is set to one hour",  # noqa: E501
        "reason": "reason to give users, default: %s" % DEFAULT_REASON,
        "kind": "kind of reboot to do. It can be 'reboot', 'halt', 'wall' or 'cancel', default: %s"
        % ShutdownType.reboot,
    },
)
def reboot_host(
    con: Context,
    force: bool = False,
    ganeti_checks: bool = True,
    ganeti_migrate: bool = True,
    ganeti_migrate_back: bool = True,
    delay_down_seconds: int = DEFAULT_DELAY_DOWN_SECONDS,
    delay_up_seconds: int = DEFAULT_DELAY_UP_SECONDS,
    delay_shutdown_minutes: int = DEFAULT_DELAY_SHUTDOWN_MINUTES,
    delay_hosts_seconds: int = DEFAULT_DELAY_HOSTS_SECONDS,
    silence_ends_at: datetime | str | None = None,
    reason: str = DEFAULT_REASON,
    kind: ShutdownType = ShutdownType.reboot,
):
    """reboot a single host

    This will reboot the given host with the right delays and
    everything.

    It's similiar to reboot.shutdown_and_wait, but covers more use
    cases like checking if the host needs a reboot first, canceled
    reboots and so on. It's split out in fleet because it's aimed to
    support rebooting multiple hosts eventually, and we want those two
    functions close together.
    """

    hostname = con.host
    if kind in (ShutdownType.wall, ShutdownType.cancel):
        shutdown(con, kind, reason=reason)
        # bypass the shutdown_and_wait complexity, at the cost of
        # reproducing a bit of shutdown_and_wait here
        if not ganeti_checks:
            return True

        try:
            master_con = find_context(getmaster(con), config=con.config)
        except invoke.exceptions.Failure:
            # not a ganeti node, ignoring
            return True
        affected_instances = _list_instances(
            master_con,
            {
                "pnode": con.host,
                "admin_state": "up",
            },
        )
        logging.info(
            "canceling shutdown on affected Ganeti VMs: %s",
            " ".join(affected_instances),
        )
        for instance in affected_instances:
            shutdown(
                find_context(instance),
                kind,
                reason=reason,
                notify=False,
            )
        return True

    logging.info("checking if host %s needs a reboot", hostname)
    if not needs_reboot(con):
        if force:
            logging.warning("rebooting anyways because of --force")
        else:
            logging.info("host %s does not need a reboot, skipping", hostname)
            return True

    logging.info("rebooting host %s", hostname)
    if not shutdown_and_wait(
        con,
        reason=reason,
        kind=kind,
        delay_down=delay_down_seconds,
        delay_up=delay_up_seconds,
        delay_shutdown=delay_shutdown_minutes,
        silence_ends_at=silence_ends_at,
        ganeti_checks=ganeti_checks,
        ganeti_migrate=ganeti_migrate,
        ganeti_migrate_back=ganeti_migrate_back,
    ):
        logging.error("rebooting host %s failed, aborting", hostname)
        return False

    logging.info("done with host %s", hostname)
    # raise the bell so we bring attention to this window
    print("\a")

    if first_reboot and delay_hosts_seconds > 0:
        now = datetime.now(timezone.utc)
        delta = timedelta(seconds=delay_hosts_seconds)
        confirm_with_timeout(
            "waiting %s before rebooting %s, now is %s waiting until %s or press enter to continue: "
            % (
                delta,
                hostname,
                now.isoformat(timespec="seconds"),
                (now + delta).isoformat(timespec="seconds"),
            ),
            delay_hosts_seconds,
        )
