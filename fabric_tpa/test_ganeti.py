from . import ganeti


instances_output_stopped = r"""checking if host dal-node-03 needs a reboot
NEEDRESTART-VER: 3.5
NEEDRESTART-KCUR: 5.10.0-23-amd64
NEEDRESTART-KEXP: 5.10.0-23-amd64
NEEDRESTART-KSTA: 1
NEEDRESTART-UCSTA: 1
NEEDRESTART-UCCUR: 0x0a0011d1
NEEDRESTART-UCEXP: 0x0a0011d1
NEEDRESTART-SVC: ganeti.service
current kernel: 5.10.0-23-amd64, expected: 5.10.0-23-amd64
current microcode: 0x0a0011d1, expected: 0x0a0011d1
reboot required: ['ganeti.service']
rebooting host dal-node-03
checking for ganeti master on host dal-node-03.torproject.org
ganeti node detected with master dal-node-01.torproject.org
ganeti node detected, migrating 7 instances from dal-node-03.torproject.org: ci-runner-x86-01.torproject.org crm-ext-01.torproject.org crm-int-01.torproject.org ns3.torproject.org probetelemetry-01.torproject.org
rdsys-frontend-01.torproject.org tb-pkgstage-01.torproject.org
sending command gnt-node migrate -f dal-node-03.torproject.org to node dal-node-01.torproject.org
Submitted jobs 85967, 85968, 85969, 85970, 85971, 85972, 85973
Waiting for job 85967 ...
Job 85967 has failed: Failure: prerequisites not met for this operation:
error type: wrong_state, error details:
Can't migrate, please use failover: Instance ci-runner-x86-01.torproject.org is not running
Waiting for job 85968 ...
Tue Aug  1 21:50:43 2023 Migrating instance rdsys-frontend-01.torproject.org
Tue Aug  1 21:50:43 2023 * checking disk consistency between source and target
Tue Aug  1 21:50:45 2023 * closing instance disks on node dal-node-02.torproject.org
Tue Aug  1 21:50:46 2023 * changing into standalone mode
Tue Aug  1 21:50:47 2023 * changing disks into dual-master mode
Tue Aug  1 21:50:49 2023 * wait until resync is done
Tue Aug  1 21:50:50 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 21:50:51 2023 * opening instance disks on node dal-node-02.torproject.org in shared mode
Tue Aug  1 21:50:52 2023 * preparing dal-node-02.torproject.org to accept the instance
Tue Aug  1 21:50:52 2023 * migrating instance to dal-node-02.torproject.org
Tue Aug  1 21:50:52 2023 * starting memory transfer
Tue Aug  1 21:51:03 2023 * memory transfer progress: 14.02 %
Tue Aug  1 21:51:06 2023 * memory transfer has switched to postcopy
Tue Aug  1 21:51:07 2023 * memory transfer complete
Tue Aug  1 21:51:07 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 21:51:08 2023 * wait until resync is done
Tue Aug  1 21:51:09 2023 * changing into standalone mode
Tue Aug  1 21:51:10 2023 * changing disks into single-master mode
Tue Aug  1 21:51:12 2023 * wait until resync is done
Tue Aug  1 21:51:13 2023 * done
Waiting for job 85969 ...
Tue Aug  1 21:51:13 2023 Migrating instance ns3.torproject.org
Tue Aug  1 21:51:13 2023 * checking disk consistency between source and target
Tue Aug  1 21:51:15 2023 * closing instance disks on node dal-node-02.torproject.org
Tue Aug  1 21:51:15 2023 * changing into standalone mode
Tue Aug  1 21:51:16 2023 * changing disks into dual-master mode
Tue Aug  1 21:51:18 2023 * wait until resync is done
Tue Aug  1 21:51:19 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 21:51:19 2023 * opening instance disks on node dal-node-02.torproject.org in shared mode
Tue Aug  1 21:51:20 2023 * preparing dal-node-02.torproject.org to accept the instance
Tue Aug  1 21:51:20 2023 * migrating instance to dal-node-02.torproject.org
Tue Aug  1 21:51:20 2023 * starting memory transfer
Tue Aug  1 21:51:31 2023 * memory transfer progress: 56.83 %
Tue Aug  1 21:51:37 2023 * memory transfer has switched to postcopy
Tue Aug  1 21:51:38 2023 * memory transfer complete
Tue Aug  1 21:51:38 2023 * closing instance disks on node dal-node-03.torproject.org                                                                                                                        [71/1693]
Tue Aug  1 21:51:39 2023 * wait until resync is done
Tue Aug  1 21:51:39 2023 * changing into standalone mode
Tue Aug  1 21:51:40 2023 * changing disks into single-master mode
Tue Aug  1 21:51:42 2023 * wait until resync is done
Tue Aug  1 21:51:42 2023 * done
Waiting for job 85970 ...
Tue Aug  1 21:51:43 2023 Migrating instance probetelemetry-01.torproject.org
Tue Aug  1 21:51:43 2023 * checking disk consistency between source and target
Tue Aug  1 21:51:45 2023 * closing instance disks on node dal-node-02.torproject.org
Tue Aug  1 21:51:46 2023 * changing into standalone mode
Tue Aug  1 21:51:47 2023 * changing disks into dual-master mode
Tue Aug  1 21:51:49 2023 * wait until resync is done
Tue Aug  1 21:51:50 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 21:51:51 2023 * opening instance disks on node dal-node-02.torproject.org in shared mode
Tue Aug  1 21:51:51 2023 * preparing dal-node-02.torproject.org to accept the instance
Tue Aug  1 21:51:52 2023 * migrating instance to dal-node-02.torproject.org
Tue Aug  1 21:51:52 2023 * starting memory transfer
Tue Aug  1 21:52:03 2023 * memory transfer progress: 28.65 %
Tue Aug  1 21:52:13 2023 * memory transfer progress: 57.13 %
Tue Aug  1 21:52:24 2023 * memory transfer progress: 85.42 %
Tue Aug  1 21:52:26 2023 * memory transfer complete
Tue Aug  1 21:52:26 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 21:52:27 2023 * wait until resync is done
Tue Aug  1 21:52:28 2023 * changing into standalone mode
Tue Aug  1 21:52:29 2023 * changing disks into single-master mode
Tue Aug  1 21:52:30 2023 * wait until resync is done
Tue Aug  1 21:52:31 2023 * done
Waiting for job 85971 ...
Tue Aug  1 21:52:32 2023 Migrating instance crm-int-01.torproject.org
Tue Aug  1 21:52:32 2023 * checking disk consistency between source and target
Tue Aug  1 21:52:34 2023 * closing instance disks on node dal-node-02.torproject.org
Tue Aug  1 21:52:34 2023 * changing into standalone mode
Tue Aug  1 21:52:36 2023 * changing disks into dual-master mode
Tue Aug  1 21:52:38 2023 * wait until resync is done
Tue Aug  1 21:52:39 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 21:52:39 2023 * opening instance disks on node dal-node-02.torproject.org in shared mode
Tue Aug  1 21:52:40 2023 * preparing dal-node-02.torproject.org to accept the instance
Tue Aug  1 21:52:40 2023 * migrating instance to dal-node-02.torproject.org
Tue Aug  1 21:52:41 2023 * starting memory transfer
Tue Aug  1 21:52:51 2023 * memory transfer progress: 14.25 %
Tue Aug  1 21:53:02 2023 * memory transfer progress: 28.56 %
Tue Aug  1 21:53:12 2023 * memory transfer progress: 42.86 %
Tue Aug  1 21:53:23 2023 * memory transfer progress: 57.18 %
Tue Aug  1 21:53:34 2023 * memory transfer progress: 71.48 %
Tue Aug  1 21:53:44 2023 * memory transfer progress: 85.76 %
Tue Aug  1 21:53:51 2023 * memory transfer has switched to postcopy
Tue Aug  1 21:53:52 2023 * memory transfer complete
Tue Aug  1 21:53:52 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 21:53:53 2023 * wait until resync is done
Tue Aug  1 21:53:54 2023 * changing into standalone mode
Tue Aug  1 21:53:55 2023 * changing disks into single-master mode
Tue Aug  1 21:53:57 2023 * wait until resync is done
Tue Aug  1 21:53:58 2023 * done
Waiting for job 85972 ...
Tue Aug  1 21:53:58 2023 Migrating instance crm-ext-01.torproject.org
Tue Aug  1 21:53:58 2023 * checking disk consistency between source and target
Tue Aug  1 21:54:00 2023 * closing instance disks on node dal-node-02.torproject.org
Tue Aug  1 21:54:00 2023 * changing into standalone mode
Tue Aug  1 21:54:01 2023 * changing disks into dual-master mode
Tue Aug  1 21:54:03 2023 * wait until resync is done
Tue Aug  1 21:54:04 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 21:54:04 2023 * opening instance disks on node dal-node-02.torproject.org in shared mode
Tue Aug  1 21:54:05 2023 * preparing dal-node-02.torproject.org to accept the instance
Tue Aug  1 21:54:05 2023 * migrating instance to dal-node-02.torproject.org
Tue Aug  1 21:54:05 2023 * starting memory transfer
Tue Aug  1 21:54:16 2023 * memory transfer progress: 56.99 %
Tue Aug  1 21:54:22 2023 * memory transfer has switched to postcopy
Tue Aug  1 21:54:23 2023 * memory transfer complete
Tue Aug  1 21:54:23 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 21:54:24 2023 * wait until resync is done
Tue Aug  1 21:54:25 2023 * changing into standalone mode
Tue Aug  1 21:54:25 2023 * changing disks into single-master mode
Tue Aug  1 21:54:27 2023 * wait until resync is done
Tue Aug  1 21:54:27 2023 * done
Waiting for job 85973 ...
Tue Aug  1 21:54:28 2023 Migrating instance tb-pkgstage-01.torproject.org
Tue Aug  1 21:54:28 2023 * checking disk consistency between source and target
Tue Aug  1 21:54:30 2023 * closing instance disks on node dal-node-02.torproject.org
Tue Aug  1 21:54:31 2023 * changing into standalone mode
Tue Aug  1 21:54:32 2023 * changing disks into dual-master mode
Tue Aug  1 21:54:34 2023 * wait until resync is done
Tue Aug  1 21:54:35 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 21:54:36 2023 * opening instance disks on node dal-node-02.torproject.org in shared mode
Tue Aug  1 21:54:37 2023 * preparing dal-node-02.torproject.org to accept the instance
Tue Aug  1 21:54:37 2023 * migrating instance to dal-node-02.torproject.org
Tue Aug  1 21:54:37 2023 * starting memory transfer
Tue Aug  1 21:54:48 2023 * memory transfer progress: 13.89 %
Tue Aug  1 21:54:51 2023 * memory transfer complete
Tue Aug  1 21:54:51 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 21:54:52 2023 * wait until resync is done
Tue Aug  1 21:54:53 2023 * changing into standalone mode
Tue Aug  1 21:54:54 2023 * changing disks into single-master mode
Tue Aug  1 21:54:56 2023 * wait until resync is done
Tue Aug  1 21:54:56 2023 * done
There were 1 errors during the node migration."""  # noqa: E501


instances_output_plain = r"""checking if host dal-node-01 needs a reboot
NEEDRESTART-VER: 3.5
NEEDRESTART-KCUR: 5.10.0-23-amd64
NEEDRESTART-KEXP: 5.10.0-23-amd64
NEEDRESTART-KSTA: 1
NEEDRESTART-UCSTA: 1
NEEDRESTART-UCCUR: 0x0a0011d1
NEEDRESTART-UCEXP: 0x0a0011d1
NEEDRESTART-SVC: ganeti.service
current kernel: 5.10.0-23-amd64, expected: 5.10.0-23-amd64
current microcode: 0x0a0011d1, expected: 0x0a0011d1
reboot required: ['ganeti.service']
rebooting host dal-node-01
checking for ganeti master on host dal-node-01.torproject.org
ganeti node detected with master dal-node-01.torproject.org
ganeti node detected, migrating 7 instances from dal-node-01.torproject.org: dangerzone-01.torproject.org donate-review-01.torproject.org forum-01.torproject.org minio-01.torproject.org static-gitlab-shim.torproje
ct.org telegram-bot-01.torproject.org web-dal-07.torproject.org
sending command gnt-node migrate -f dal-node-01.torproject.org to node dal-node-01.torproject.org
Submitted jobs 85887, 85888, 85889, 85890, 85891, 85892, 85893
Waiting for job 85887 ...
Tue Aug  1 20:51:45 2023 Migrating instance dangerzone-01.torproject.org
Tue Aug  1 20:51:45 2023 * checking disk consistency between source and target
Tue Aug  1 20:51:46 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 20:51:47 2023 * changing into standalone mode
Tue Aug  1 20:51:47 2023 * changing disks into dual-master mode
Tue Aug  1 20:51:49 2023 * wait until resync is done
Tue Aug  1 20:51:50 2023 * opening instance disks on node dal-node-01.torproject.org in shared mode
Tue Aug  1 20:51:50 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 20:51:51 2023 * preparing dal-node-03.torproject.org to accept the instance
Tue Aug  1 20:51:51 2023 * migrating instance to dal-node-03.torproject.org
Tue Aug  1 20:51:51 2023 * starting memory transfer
Tue Aug  1 20:52:02 2023 * memory transfer progress: 14.04 %
Tue Aug  1 20:52:06 2023 * memory transfer has switched to postcopy
Tue Aug  1 20:52:08 2023 * memory transfer complete
Tue Aug  1 20:52:08 2023 * closing instance disks on node dal-node-01.torproject.org
Tue Aug  1 20:52:09 2023 * wait until resync is done
Tue Aug  1 20:52:10 2023 * changing into standalone mode
Tue Aug  1 20:52:10 2023 * changing disks into single-master mode
Tue Aug  1 20:52:12 2023 * wait until resync is done
Tue Aug  1 20:52:12 2023 * done
Waiting for job 85888 ...
Tue Aug  1 20:52:13 2023 Migrating instance static-gitlab-shim.torproject.org
Tue Aug  1 20:52:13 2023 * checking disk consistency between source and target
Tue Aug  1 20:52:15 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 20:52:16 2023 * changing into standalone mode
Tue Aug  1 20:52:17 2023 * changing disks into dual-master mode
Tue Aug  1 20:52:19 2023 * wait until resync is done
Tue Aug  1 20:52:20 2023 * opening instance disks on node dal-node-01.torproject.org in shared mode
Tue Aug  1 20:52:20 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 20:52:21 2023 * preparing dal-node-03.torproject.org to accept the instance
Tue Aug  1 20:52:21 2023 * migrating instance to dal-node-03.torproject.org
Tue Aug  1 20:52:21 2023 * starting memory transfer
Tue Aug  1 20:52:32 2023 * memory transfer progress: 13.90 %
Tue Aug  1 20:52:43 2023 * memory transfer progress: 28.19 %
Tue Aug  1 20:52:53 2023 * memory transfer has switched to postcopy
Tue Aug  1 20:52:53 2023 * memory transfer progress: 42.54 %
Tue Aug  1 20:52:54 2023 * memory transfer complete
Tue Aug  1 20:52:55 2023 * closing instance disks on node dal-node-01.torproject.org
Tue Aug  1 20:52:56 2023 * wait until resync is done
Tue Aug  1 20:52:56 2023 * changing into standalone mode
Tue Aug  1 20:52:57 2023 * changing disks into single-master mode
Tue Aug  1 20:53:00 2023 * wait until resync is done
Tue Aug  1 20:53:00 2023 * done
Waiting for job 85889 ...
Tue Aug  1 20:53:01 2023 Migrating instance forum-01.torproject.org
Tue Aug  1 20:53:01 2023 * checking disk consistency between source and target
Tue Aug  1 20:53:03 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 20:53:04 2023 * changing into standalone mode
Tue Aug  1 20:53:05 2023 * changing disks into dual-master mode
Tue Aug  1 20:53:07 2023 * wait until resync is done
Tue Aug  1 20:53:08 2023 * opening instance disks on node dal-node-01.torproject.org in shared mode
Tue Aug  1 20:53:09 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 20:53:09 2023 * preparing dal-node-03.torproject.org to accept the instance
Tue Aug  1 20:53:10 2023 * migrating instance to dal-node-03.torproject.org
Tue Aug  1 20:53:10 2023 * starting memory transfer
Tue Aug  1 20:53:21 2023 * memory transfer progress: 14.17 %
Tue Aug  1 20:53:31 2023 * memory transfer progress: 28.44 %
Tue Aug  1 20:53:42 2023 * memory transfer progress: 42.66 %
Tue Aug  1 20:53:52 2023 * memory transfer progress: 56.95 %
Tue Aug  1 20:54:03 2023 * memory transfer progress: 71.16 %
Tue Aug  1 20:54:14 2023 * memory transfer progress: 85.41 %
Tue Aug  1 20:54:17 2023 * memory transfer has switched to postcopy
Tue Aug  1 20:54:21 2023 * memory transfer complete
Tue Aug  1 20:54:21 2023 * closing instance disks on node dal-node-01.torproject.org
Tue Aug  1 20:54:22 2023 * wait until resync is done
Tue Aug  1 20:54:23 2023 * changing into standalone mode
Tue Aug  1 20:54:24 2023 * changing disks into single-master mode
Tue Aug  1 20:54:26 2023 * wait until resync is done
Tue Aug  1 20:54:27 2023 * done
Waiting for job 85890 ...
Job 85890 has failed: Failure: prerequisites not met for this operation:
error type: wrong_state, error details:
Instance's disk layout 'plain' does not allow migrations
Waiting for job 85891 ...
Tue Aug  1 20:54:27 2023 * checking disk consistency between source and target
Tue Aug  1 20:54:27 2023 Migrating instance donate-review-01.torproject.org
Tue Aug  1 20:54:29 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 20:54:29 2023 * changing into standalone mode
Tue Aug  1 20:54:30 2023 * changing disks into dual-master mode
Tue Aug  1 20:54:32 2023 * wait until resync is done
Tue Aug  1 20:54:32 2023 * opening instance disks on node dal-node-01.torproject.org in shared mode
Tue Aug  1 20:54:33 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 20:54:33 2023 * preparing dal-node-03.torproject.org to accept the instance
Tue Aug  1 20:54:34 2023 * migrating instance to dal-node-03.torproject.org
Tue Aug  1 20:54:34 2023 * starting memory transfer
Tue Aug  1 20:54:44 2023 * memory transfer progress: 28.32 %
Tue Aug  1 20:54:49 2023 * memory transfer complete
Tue Aug  1 20:54:49 2023 * closing instance disks on node dal-node-01.torproject.org
Tue Aug  1 20:54:49 2023 * wait until resync is done
Tue Aug  1 20:54:50 2023 * changing into standalone mode
Tue Aug  1 20:54:51 2023 * changing disks into single-master mode
Tue Aug  1 20:54:52 2023 * wait until resync is done
Tue Aug  1 20:54:53 2023 * done
Waiting for job 85892 ...
Tue Aug  1 20:54:53 2023 Migrating instance web-dal-07.torproject.org
Tue Aug  1 20:54:53 2023 * checking disk consistency between source and target
Tue Aug  1 20:54:55 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 20:54:56 2023 * changing into standalone mode
Tue Aug  1 20:54:57 2023 * changing disks into dual-master mode
Tue Aug  1 20:54:59 2023 * wait until resync is done
Tue Aug  1 20:55:00 2023 * opening instance disks on node dal-node-01.torproject.org in shared mode
Tue Aug  1 20:55:01 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 20:55:02 2023 * preparing dal-node-03.torproject.org to accept the instance
Tue Aug  1 20:55:02 2023 * migrating instance to dal-node-03.torproject.org
Tue Aug  1 20:55:02 2023 * starting memory transfer
Tue Aug  1 20:55:13 2023 * memory transfer progress: 14.28 %
Tue Aug  1 20:55:23 2023 * memory transfer progress: 28.31 %
Tue Aug  1 20:55:34 2023 * memory transfer progress: 42.58 %
Tue Aug  1 20:55:45 2023 * memory transfer progress: 56.85 %
Tue Aug  1 20:55:55 2023 * memory transfer progress: 71.08 %
Tue Aug  1 20:56:06 2023 * memory transfer progress: 85.40 %
Tue Aug  1 20:56:15 2023 * memory transfer has switched to postcopy
Tue Aug  1 20:56:16 2023 * memory transfer progress: 99.48 %
Tue Aug  1 20:56:18 2023 * memory transfer complete
Tue Aug  1 20:56:19 2023 * closing instance disks on node dal-node-01.torproject.org
Tue Aug  1 20:56:20 2023 * wait until resync is done
Tue Aug  1 20:56:20 2023 * changing into standalone mode
Tue Aug  1 20:56:21 2023 * changing disks into single-master mode
Tue Aug  1 20:56:23 2023 * wait until resync is done
Tue Aug  1 20:56:24 2023 * done
Waiting for job 85893 ...
Tue Aug  1 20:56:24 2023 Migrating instance telegram-bot-01.torproject.org
Tue Aug  1 20:56:25 2023 * checking disk consistency between source and target
Tue Aug  1 20:56:25 2023 * closing instance disks on node dal-node-03.torproject.org
Tue Aug  1 20:56:26 2023 * changing into standalone mode
Tue Aug  1 20:56:26 2023 * changing disks into dual-master mode
Tue Aug  1 20:56:28 2023 * wait until resync is done
Tue Aug  1 20:56:28 2023 * opening instance disks on node dal-node-01.torproject.org in shared mode
Tue Aug  1 20:56:28 2023 * opening instance disks on node dal-node-03.torproject.org in shared mode
Tue Aug  1 20:56:29 2023 * preparing dal-node-03.torproject.org to accept the instance
Tue Aug  1 20:56:29 2023 * migrating instance to dal-node-03.torproject.org
Tue Aug  1 20:56:29 2023 * starting memory transfer
Tue Aug  1 20:56:40 2023 * memory transfer progress: 14.04 %
Tue Aug  1 20:56:42 2023 * memory transfer has switched to postcopy
Tue Aug  1 20:56:43 2023 * memory transfer complete
Tue Aug  1 20:56:43 2023 * closing instance disks on node dal-node-01.torproject.org
Tue Aug  1 20:56:44 2023 * wait until resync is done
Tue Aug  1 20:56:44 2023 * changing into standalone mode
Tue Aug  1 20:56:44 2023 * changing disks into single-master mode
Tue Aug  1 20:56:46 2023 * wait until resync is done
Tue Aug  1 20:56:46 2023 * done
There were 1 errors during the node migration."""  # noqa: E501


instances_output_two_plain = """checking if host dal-node-01.torproject.org needs a reboot
NEEDRESTART-VER: 3.5
NEEDRESTART-KCUR: 5.10.0-26-amd64
NEEDRESTART-KEXP: 5.10.0-27-amd64
NEEDRESTART-KSTA: 3
NEEDRESTART-UCSTA: 1
NEEDRESTART-UCCUR: 0x0a0011d1
NEEDRESTART-UCEXP: 0x0a0011d1
current kernel: 5.10.0-26-amd64, expected: 5.10.0-27-amd64
current microcode: 0x0a0011d1, expected: 0x0a0011d1
reboot required: ['kernel']
rebooting host dal-node-01.torproject.org
found account ('TPA', '', 'jP}JN_glQa5dr.c]') in .netrc file, using it for IRC notifications
checking for ganeti master on host dal-node-01.torproject.org
ganeti node detected with master dal-node-01.torproject.org
ganeti node detected, master is dal-node-01.torproject.org
migrating 8 instances from dal-node-01.torproject.org: ci-runner-x86-02.torproject.org dangerzone-01.torproject.org donate-review-01.torproject.org forum-01.torproject.org minio-01.torproject.org static-gitlab-shim.torproject.org telegram-bot-01.torproject.org web-dal-07.torproject.org
found 2 plain instances to shut down after migration
sending command gnt-node migrate -f dal-node-01.torproject.org to node dal-node-01.torproject.org
Submitted jobs 178337, 178338, 178339, 178340, 178341, 178342, 178343, 178344
Waiting for job 178337 ...
Mon Jan  8 15:11:19 2024 Migrating instance dangerzone-01.torproject.org
Mon Jan  8 15:11:19 2024 * checking disk consistency between source and target
Mon Jan  8 15:11:20 2024 * closing instance disks on node dal-node-03.torproject.org
Mon Jan  8 15:11:21 2024 * changing into standalone mode
Mon Jan  8 15:11:21 2024 * changing disks into dual-master mode
Mon Jan  8 15:11:23 2024 * wait until resync is done
Mon Jan  8 15:11:23 2024 * opening instance disks on node dal-node-01.torproject.org in shared mode
Mon Jan  8 15:11:24 2024 * opening instance disks on node dal-node-03.torproject.org in shared mode
Mon Jan  8 15:11:24 2024 * preparing dal-node-03.torproject.org to accept the instance
Mon Jan  8 15:11:25 2024 * migrating instance to dal-node-03.torproject.org
Mon Jan  8 15:11:25 2024 * starting memory transfer
Mon Jan  8 15:11:36 2024 * memory transfer progress: 14.03 %
Mon Jan  8 15:11:46 2024 * memory transfer progress: 28.30 %
Mon Jan  8 15:11:57 2024 * memory transfer progress: 42.57 %
Mon Jan  8 15:12:02 2024 * memory transfer has switched to postcopy
Mon Jan  8 15:12:03 2024 * memory transfer complete
Mon Jan  8 15:12:04 2024 * closing instance disks on node dal-node-01.torproject.org
Mon Jan  8 15:12:04 2024 * wait until resync is done
Mon Jan  8 15:12:05 2024 * changing into standalone mode
Mon Jan  8 15:12:06 2024 * changing disks into single-master mode
Mon Jan  8 15:12:07 2024 * wait until resync is done
Mon Jan  8 15:12:08 2024 * done
Waiting for job 178338 ...
Job 178338 has failed: Failure: prerequisites not met for this operation:
error type: wrong_state, error details:
Instance's disk layout 'plain' does not allow migrations
Waiting for job 178339 ...
Mon Jan  8 15:12:08 2024 Migrating instance static-gitlab-shim.torproject.org
Mon Jan  8 15:12:08 2024 * checking disk consistency between source and target
Mon Jan  8 15:12:10 2024 * closing instance disks on node dal-node-03.torproject.org
Mon Jan  8 15:12:11 2024 * changing into standalone mode
Mon Jan  8 15:12:12 2024 * changing disks into dual-master mode
Mon Jan  8 15:12:14 2024 * wait until resync is done
Mon Jan  8 15:12:14 2024 * opening instance disks on node dal-node-01.torproject.org in shared mode
Mon Jan  8 15:12:15 2024 * opening instance disks on node dal-node-03.torproject.org in shared mode
Mon Jan  8 15:12:16 2024 * preparing dal-node-03.torproject.org to accept the instance
Mon Jan  8 15:12:16 2024 * migrating instance to dal-node-03.torproject.org
Mon Jan  8 15:12:16 2024 * starting memory transfer
Mon Jan  8 15:12:27 2024 * memory transfer progress: 14.04 %
Mon Jan  8 15:12:37 2024 * memory transfer progress: 28.28 %
Mon Jan  8 15:12:48 2024 * memory transfer progress: 42.58 %
Mon Jan  8 15:12:59 2024 * memory transfer progress: 56.78 %
Mon Jan  8 15:13:08 2024 * memory transfer complete
Mon Jan  8 15:13:08 2024 * closing instance disks on node dal-node-01.torproject.org
Mon Jan  8 15:13:09 2024 * wait until resync is done
Mon Jan  8 15:13:10 2024 * changing into standalone mode
Mon Jan  8 15:13:11 2024 * changing disks into single-master mode
Mon Jan  8 15:13:13 2024 * wait until resync is done
Mon Jan  8 15:13:14 2024 * done
Waiting for job 178340 ...
Mon Jan  8 15:13:14 2024 Migrating instance forum-01.torproject.org
Mon Jan  8 15:13:14 2024 * checking disk consistency between source and target
Mon Jan  8 15:13:16 2024 * closing instance disks on node dal-node-03.torproject.org
Mon Jan  8 15:13:17 2024 * changing into standalone mode
Mon Jan  8 15:13:18 2024 * changing disks into dual-master mode
Mon Jan  8 15:13:19 2024 * wait until resync is done
Mon Jan  8 15:13:20 2024 * opening instance disks on node dal-node-01.torproject.org in shared mode
Mon Jan  8 15:13:21 2024 * opening instance disks on node dal-node-03.torproject.org in shared mode
Mon Jan  8 15:13:22 2024 * preparing dal-node-03.torproject.org to accept the instance
Mon Jan  8 15:13:22 2024 * migrating instance to dal-node-03.torproject.org
Mon Jan  8 15:13:22 2024 * starting memory transfer
Mon Jan  8 15:13:33 2024 * memory transfer progress: 14.27 %
Mon Jan  8 15:13:44 2024 * memory transfer progress: 28.48 %
Mon Jan  8 15:13:54 2024 * memory transfer progress: 42.66 %
Mon Jan  8 15:14:05 2024 * memory transfer progress: 56.65 %
Mon Jan  8 15:14:15 2024 * memory transfer progress: 70.87 %
Mon Jan  8 15:14:26 2024 * memory transfer progress: 84.93 %
Mon Jan  8 15:14:32 2024 * memory transfer has switched to postcopy
Mon Jan  8 15:14:36 2024 * memory transfer complete
Mon Jan  8 15:14:36 2024 * closing instance disks on node dal-node-01.torproject.org
Mon Jan  8 15:14:37 2024 * wait until resync is done
Mon Jan  8 15:14:38 2024 * changing into standalone mode
Mon Jan  8 15:14:39 2024 * changing disks into single-master mode
Mon Jan  8 15:14:41 2024 * wait until resync is done
Mon Jan  8 15:14:42 2024 * done
Waiting for job 178341 ...
Job 178341 has failed: Failure: prerequisites not met for this operation:
error type: wrong_state, error details:
Instance's disk layout 'plain' does not allow migrations
Waiting for job 178342 ...
Mon Jan  8 15:14:42 2024 Migrating instance donate-review-01.torproject.org
Mon Jan  8 15:14:42 2024 * checking disk consistency between source and target
Mon Jan  8 15:14:44 2024 * closing instance disks on node dal-node-03.torproject.org
Mon Jan  8 15:14:44 2024 * changing into standalone mode
Mon Jan  8 15:14:45 2024 * changing disks into dual-master mode
Mon Jan  8 15:14:46 2024 * wait until resync is done
Mon Jan  8 15:14:47 2024 * opening instance disks on node dal-node-01.torproject.org in shared mode
Mon Jan  8 15:14:47 2024 * opening instance disks on node dal-node-03.torproject.org in shared mode
Mon Jan  8 15:14:48 2024 * preparing dal-node-03.torproject.org to accept the instance
Mon Jan  8 15:14:48 2024 * migrating instance to dal-node-03.torproject.org
Mon Jan  8 15:14:48 2024 * starting memory transfer
Mon Jan  8 15:14:59 2024 * memory transfer progress: 28.00 %
Mon Jan  8 15:15:08 2024 * memory transfer complete
Mon Jan  8 15:15:08 2024 * closing instance disks on node dal-node-01.torproject.org
Mon Jan  8 15:15:09 2024 * wait until resync is done
Mon Jan  8 15:15:10 2024 * changing into standalone mode
Mon Jan  8 15:15:10 2024 * changing disks into single-master mode
Mon Jan  8 15:15:12 2024 * wait until resync is done
Mon Jan  8 15:15:12 2024 * done
Waiting for job 178343 ...
Mon Jan  8 15:15:13 2024 Migrating instance web-dal-07.torproject.org
Mon Jan  8 15:15:13 2024 * checking disk consistency between source and target
Mon Jan  8 15:15:15 2024 * closing instance disks on node dal-node-03.torproject.org
Mon Jan  8 15:15:16 2024 * changing into standalone mode
Mon Jan  8 15:15:16 2024 * changing disks into dual-master mode
Mon Jan  8 15:15:18 2024 * wait until resync is done
Mon Jan  8 15:15:19 2024 * opening instance disks on node dal-node-01.torproject.org in shared mode
Mon Jan  8 15:15:20 2024 * opening instance disks on node dal-node-03.torproject.org in shared mode
Mon Jan  8 15:15:21 2024 * preparing dal-node-03.torproject.org to accept the instance
Mon Jan  8 15:15:21 2024 * migrating instance to dal-node-03.torproject.org
Mon Jan  8 15:15:21 2024 * starting memory transfer
Mon Jan  8 15:15:32 2024 * memory transfer progress: 14.26 %
Mon Jan  8 15:15:42 2024 * memory transfer progress: 28.58 %
Mon Jan  8 15:15:53 2024 * memory transfer progress: 42.88 %
Mon Jan  8 15:16:04 2024 * memory transfer progress: 57.18 %
Mon Jan  8 15:16:14 2024 * memory transfer progress: 71.48 %
Mon Jan  8 15:16:25 2024 * memory transfer progress: 85.76 %
Mon Jan  8 15:16:33 2024 * memory transfer has switched to postcopy
Mon Jan  8 15:16:35 2024 * memory transfer progress: 99.95 %
Mon Jan  8 15:16:38 2024 * memory transfer complete
Mon Jan  8 15:16:38 2024 * closing instance disks on node dal-node-01.torproject.org
Mon Jan  8 15:16:39 2024 * wait until resync is done
Mon Jan  8 15:16:39 2024 * changing into standalone mode
Mon Jan  8 15:16:40 2024 * changing disks into single-master mode
Mon Jan  8 15:16:42 2024 * wait until resync is done
Mon Jan  8 15:16:43 2024 * done
Waiting for job 178344 ...
Mon Jan  8 15:16:43 2024 Migrating instance telegram-bot-01.torproject.org
Mon Jan  8 15:16:43 2024 * checking disk consistency between source and target
Mon Jan  8 15:16:44 2024 * closing instance disks on node dal-node-03.torproject.org
Mon Jan  8 15:16:44 2024 * changing into standalone mode
Mon Jan  8 15:16:45 2024 * changing disks into dual-master mode
Mon Jan  8 15:16:46 2024 * wait until resync is done
Mon Jan  8 15:16:47 2024 * opening instance disks on node dal-node-01.torproject.org in shared mode
Mon Jan  8 15:16:47 2024 * opening instance disks on node dal-node-03.torproject.org in shared mode
Mon Jan  8 15:16:47 2024 * preparing dal-node-03.torproject.org to accept the instance
Mon Jan  8 15:16:47 2024 * migrating instance to dal-node-03.torproject.org
Mon Jan  8 15:16:47 2024 * starting memory transfer
Mon Jan  8 15:16:58 2024 * memory transfer progress: 14.10 %
Mon Jan  8 15:17:09 2024 * memory transfer progress: 28.36 %
Mon Jan  8 15:17:18 2024 * memory transfer has switched to postcopy
Mon Jan  8 15:17:19 2024 * memory transfer complete
Mon Jan  8 15:17:20 2024 * closing instance disks on node dal-node-01.torproject.org
Mon Jan  8 15:17:20 2024 * wait until resync is done
Mon Jan  8 15:17:20 2024 * changing into standalone mode
Mon Jan  8 15:17:21 2024 * changing disks into single-master mode
Mon Jan  8 15:17:22 2024 * wait until resync is done
Mon Jan  8 15:17:22 2024 * done
There were 2 errors during the node migration.
unexpected exception during reboot: [Exit('failed to empty node dal-node-01.torproject.org, aborting')] failed to empty node dal-node-01.torproject.org, aborting
"""  # noqa: E501


instances_output_empty = (
    "No primary instances on node fsn-node-03.torproject.org, exiting."
)


def test_instances_output():
    assert ganeti.process_migrate_log(instances_output_stopped, False, None)
    assert ganeti.process_migrate_log(instances_output_plain, False, None)
    assert ganeti.process_migrate_log(instances_output_two_plain, False, None)
    assert ganeti.process_migrate_log(instances_output_empty, True, None)
