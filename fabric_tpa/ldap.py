import getpass
import logging
import os.path
import sys


from invoke.exceptions import Exit

try:
    import ldap
except ImportError:
    sys.stderr.write(
        "cannot find Python LDAP, install with `apt install python3-ldap`\n"
    )
    raise


from . import local


class LdapContext(object):
    """The LdapContext is a more pythonic wrapper around the python-ldap module

    It is very opinionated: it will setup TLS with a hardcoded
    certificate, for example, and hardcodes a binding domain. It will
    also prompt for a passphrase and does a search on SUBTREE.

    The point is to remove much of the LDAP intricacies from the
    caller so they don't have to know as much of the complexity of the
    protocol to do simple things. Obviously, the abstraction is leaky
    as we don't hide stuff like the filtering language or DNs.

    This also does not catch most exceptions that might be generated
    by LDAP. Callers should watch out for ldap.LDAPError, or errors
    documented at:
    https://www.python-ldap.org/en/python-ldap-3.2.0/reference/ldap.html#exceptions

    .. todo: implement object modification. take example on
    ``ud-arbimport``, ``ud-host``, or ``ud-useradd`` in
    userdir-ldap.
    """

    # the default URI if not specified
    default_uri = "ldaps://db.torproject.org"
    # the base domain name for this domain, used in authentication and
    # search
    base_dn = "dc=torproject,dc=org"
    base_dn_users = "ou=users," + base_dn
    base_dn_hosts = "ou=hosts," + base_dn
    # how to construct a guessed username if not provided. the %s is
    # interpolated by bind() with getuser()
    base_dn_user_template = "uid=%s," + base_dn_users
    # the certificate to use to verify with the LDAP server
    tls_cacertfile = os.path.dirname(__file__) + "/db.torproject.org.pem"

    def __init__(self, uri=None):
        """initialize the LdapContext

        This initializes an `ldap` object from the given URI, which
        SHOULD have a ldaps:// prefix.

        It will also setup TLS using a hardcoded certificate and
        enforce it.

        """
        if uri is None:
            uri = self.default_uri
        self.uri = uri
        self.ldap = ldap.initialize(uri)
        # TODO: certificate might expire, check for expiry and renew
        # if necessary
        self.ldap.set_option(
            ldap.OPT_X_TLS_CACERTFILE,
            self.tls_cacertfile,
        )
        # default, but just making sure
        self.ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_HARD)

    def bind(self, dn=None, password=None):
        """bind to the initialized LDAP connection using the provided DN and
        password

        If the dn is None (default), it is guessed based on the
        current local user.

        If the `password` is None (default), it is prompted using the
        `getpass` library.

        This also sets the `dn` member value for debugging purposes.

        As an exception, this function *does* catch a few common
        exceptions that will be triggered by a default
        configuration. In particular, this will fail if the user
        cannot access the LDAP server.
        """
        if dn is None:
            dn = self.base_dn_user_template % os.environ.get(
                "LDAP_USER", local.getuser()
            )
        self.dn = dn
        if password is None:
            password = getpass.getpass(
                prompt="%s LDAP password for %s: " % (self.uri, dn)
            )
        try:
            self.ldap.simple_bind_s(dn, password)
        except ldap.SERVER_DOWN as e:
            # port not open or TLS failure
            raise Exit("failed to contact LDAP server, firewall problems? %s" % e)
        except ldap.UNWILLING_TO_PERFORM as e:
            # user has tried to connect over cleartext
            raise Exit("failed to contact LDAP server, cleartext fail?" % e)
        # allow chaining (e.g. `l = LdapContext().bind()`)
        return self

    def search(self, filterstr="(objectClass=*)", base=None):
        """Search the given base for the filterstr"""
        if base is None:
            base = self.base_dn
        logging.debug("searching for %r inside %r", filterstr, base)
        return self.ldap.search_s(
            base=base,
            filterstr=filterstr,
            scope=ldap.SCOPE_SUBTREE,
        )

    def search_users(self, filterstr="(objectClass=*)", base=None):
        """Search for users matching the filter string (filterstr)

        This is a wrapper around search but with the default `base`
        set to the the preconfigured base_dn_users,
        e.g. ou=users,dc=example,dc=com.
        """
        if base is None:
            base = self.base_dn_users
        return self.search(filterstr=filterstr, base=base)

    def search_hosts(self, filterstr="(objectClass=*)", base=None):
        """Search for hosts matching the filter string (filterstr)

        This is a wrapper around search but with the default `base`
        set to the the preconfigured base_dn_hosts,
        e.g. ou=hosts,dc=example,dc=com.
        """
        if base is None:
            base = self.base_dn_hosts
        return self.search(filterstr=filterstr, base=base)

    def load_host(self, hostname):
        """load the attributes of a single host

        This is a wrapper around search_hosts that makes sure we only
        match one host.

        It is the caller's responsability to ensure that the hostname
        provide is an non-ambiguous FQDN but this will show an error
        on the console if more than one results are returened.

        Returns a tuple made of the matched distinguished name (dn)
        and the host's attributes.
        """
        filter = "(hostname=%s)" % hostname
        found = False
        host = ()
        for dn, attrs in self.search_hosts(filterstr=filter):
            logging.debug("dn: %s, attrs: %r" % (dn, attrs))
            if found:
                logging.warning("discarding extra matches for hostname %s", hostname)
                break
            else:
                host = dn, attrs
            found = True
        return host

    def __str__(self):
        """string representation of this object

        This censors the password, which is not kept, for security reasons."""
        return "LdapContext(%r, %r, %r): %s" % (
            self.uri,
            self.dn,
            "[CENSORED]",
            self.ldap,
        )
