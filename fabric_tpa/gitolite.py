#!/usr/bin/python3
# coding: utf-8

"""Code to operate the Tor Gitolite server.

This code was mostly grown as part of the Gitolite to GitLab migration
operated in 2024 by anarcat, as part of TPA-RFC-36:

https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-36-gitolite-gitweb-retirement

It *may* be useful for others, but was organically grown as the
migration was performed and might not actually work when doing a
migration from scratch, as it was heavily modified as time went on. It
should still be relatively solid code: the final migration was done by
running the same command (`fab -H cupani.torproject.org
mass-repos-migration`) repeatedly and it still worked until the very
end.

Most of it is idempotent, but not all: a GitLab repository sometimes
needs to be deleted for the migration to proceed correctly. Close
attention need to be paid to the output to avoid data loss.

The mass migration code for user repositories is particularly hairy.

Since then, however, the code was refactored and cleaned up, so it's
possible some bugs crept back in.

In general, if you reuse this in your own migration, watch out for
hardcoded paths and URLs. I've tried to keep those as customizable
command-line options, but there is definitely some hardcoded logic in
there, like hostnames, URLs and paths. Grep for XXX.

It is strongly recommended to run the migration under a logger like
script(1) or ttyrec(1) to keep a log of what is actually been done by
the script.

More information in the tracking issue:

https://gitlab.torproject.org/tpo/tpa/team/-/issues/41215
"""

import logging
import os.path
import re
import subprocess
import sys
import tempfile
import time
from datetime import datetime, timedelta
from io import StringIO
from os import PathLike
from pathlib import Path
from typing import Iterator, Optional, Union, cast

try:
    from fabric import Connection, task
except ImportError:
    sys.stderr.write("cannot find fabric, install with `apt install python3-fabric`")
    raise
import gitlab.const
import gitlab.exceptions
import requests
from invoke import Context, Exit
from paramiko.ssh_exception import SSHException

from fabric_tpa.git import commit_and_push, is_empty_repo, list_all_repos
from fabric_tpa.gitlab import GitLabConnector, create_project
from fabric_tpa.host import schedule_delete
from fabric_tpa.ui import re_include_exclude, Timer, no_yes, pick, yes_no


@task
def list_conf_repos(
    con: Connection,
    local_conf_dir: str = "~/src/tor/gitolite-admin",
):
    """list repos in gitolite config"""
    conf_path = Path(local_conf_dir).expanduser() / "conf" / "gitolite.conf"
    for repo in list_conf_repos_str(Path(conf_path).expanduser().open().read()):
        print(repo)


def list_conf_repos_str(content: str):
    """help function to parse the gitolite.conf contents and spew out projects

    This is a separate function to make unit testing easier."""
    # copy-paste from rewrite_gitolite_conf_category
    for line in content.splitlines():
        if "@all" in line:
            continue
        # detect start of block for the matchin repo
        if m := re.match(r"^repo +(.*) *$", line):
            yield m.group(1)


@task
def list_repos_access(
    con: Connection,
    local_conf_dir: str = "~/src/tor/gitolite-admin",
):
    """list repos access controls"""
    conf_path = Path(local_conf_dir).expanduser() / "conf" / "gitolite.conf"
    for repo, accesses in list_repos_access_str(
        Path(conf_path).expanduser().open().readlines()
    ):
        print(repo, accesses)


def list_repos_access_str(
    lines: list[str],
) -> Iterator[tuple[str, list[tuple[str, str]]]]:
    """generate a list of repos access controls, split to ease unit tests"""
    project = None
    accesses: list[tuple[str, str]] = []
    for line in lines:
        # detect start of block for the matchin repo
        if m := re.match(r"^repo +(.*) *$", line):
            # yield previously parsed project accesses
            if project:
                yield project, accesses
                accesses = []
            project = m.group(1)
            accesses = []
            continue
        if m := re.match(r"\s+([RW+]+)\s+=\s+(.*)\s*", line):
            assert project, "oops, parsed an access line without project?"
            accesses.append((m.group(1), m.group(2)))
    if project:
        yield project, accesses


@task
def list_repos_categories(
    con: Connection,
    local_conf_dir: str = "~/src/tor/gitolite-admin",
    remap_categories: bool = False,
):
    """show gitolite repositories listed by category"""
    conf_path = Path(local_conf_dir).expanduser() / "conf" / "gitolite.conf"
    category_map = map_repos_categories_str(conf_path.open().read(), remap_categories)
    for category, projects in category_map.items():
        print("%s: %s" % (category, " ".join(projects)))


@task
def count_repos_categories(
    con: Connection,
    local_conf_dir: str = "~/src/tor/gitolite-admin",
    remap_categories: bool = False,
):
    """count gitolite repositories per category"""
    conf_path = Path(local_conf_dir).expanduser() / "conf" / "gitolite.conf"
    category_map = map_repos_categories_str(conf_path.open().read(), remap_categories)
    total = 0
    for category, projects in category_map.items():
        print("%d %s" % (len(projects), category))
        total += len(projects)
    print("%d total" % total)


def remap_category(category):
    """regroup categories in more meaningful terms as per TPA-RFC-36"""
    if category is None:
        return "Other"
    for search in ("Attic", "User", "Migrated"):
        if search in category:
            return search
    for search in ("Extern", "Mirror"):
        if search in category:
            return "Extern / Mirror"
    if "Infrastructure" in category:
        return "TPA"
    return "Other"


def map_repos_categories_str(content: str, remap: bool) -> dict[str, list[str]]:
    """parse the given gitolite config file and spew out a dict of category => list list of projects"""
    category_map: dict[str, list[str]] = {}
    for repo, category in _list_conf_repos_categories_str(content):
        if remap:
            category = remap_category(category)
        if category not in category_map:
            category_map[category] = []
        category_map[category].append(repo)
    return category_map


def _list_conf_repos_categories_str(content: str):
    """helper function for the categories map builder

    This is separate to ease the implementation, using a generator
    instead of some more elaborate data structure.
    """
    project = None
    category = None
    for line in content.splitlines():
        if "@all" in line:
            continue
        # detect start of block for the matchin repo
        if m := re.match(r"^repo +(.*) *$", line):
            if project:
                yield project, category
                category = None
            project = m.group(1)
            continue
        # if this is a category line
        if project:
            if m := re.match(r"^ +config +gitweb.category += +(.*)$", line):
                category = m.group(1)
                continue

    if project:
        yield project, category


@task
def change_category(
    con: Connection,
    conf_path: Union[str, PathLike[str]],
    project: str,
    category: str,
):
    """change project category"""
    conf_path = Path(conf_path)
    with conf_path.open() as fp:
        old_content = fp.read()

    logging.info(
        "rewriting gitolite config %s to change project %s to category %s",
        conf_path,
        project,
        category,
    )
    logging.debug("old content: %r", old_content)
    new_content = "\n".join(change_category_str(old_content, project, category)) + "\n"
    logging.debug("new content: %r", old_content)
    if old_content == new_content:
        logging.warning("no change to gitolite.conf, wrong project or already fixed?")
        return False
    with conf_path.open("w") as fp:
        fp.write(new_content)
    return True


def change_category_str(content: str, project: str, category: str) -> Iterator[str]:
    """rewrite gitolite configuration string to change category of given project"""
    in_project = False
    found_category = False
    for line in content.splitlines():
        # detect start of block for the matchin repo
        if re.match(rf"^repo +{project} *$", line):
            in_project = True
            found_category = False
            yield line
            continue
        # if this is a category line
        if in_project and re.match(r"^ +config +gitweb.category +.*$", line):
            yield f"    config gitweb.category = {category}"
            found_category = True
            continue
        # detect end of the block
        #
        # this regex is not a comment or "repo " directive means end of the block
        if (
            in_project
            and not line.startswith("repo ")
            and not re.match(r"^[ #].*", line)
        ):
            if not found_category:
                yield f"    config gitweb.category = {category}"
            in_project = False
            found_category = False
            yield line
            continue
        yield line
    if in_project and not found_category:
        yield f"    config gitweb.category = {category}"


@task
def write_pre_receive_hook(con: Connection, project: str, pre_receive_str: str):
    """write given pre-receive hook for project"""
    pre_receive_stream = StringIO(pre_receive_str)
    logging.debug("uploading %r to %s", pre_receive_str, con.host)
    # XXX: hardcoded path
    pre_receive_path = (
        f"/srv/git.torproject.org/repositories/{project}.git/hooks/pre-receive"
    )
    logging.info("uploading %d bytes to %s", len(pre_receive_str), pre_receive_path)
    try:
        con.put(pre_receive_stream, pre_receive_path)
        logging.info("making %s executable", pre_receive_path)
        con.sftp().chmod(pre_receive_path, 0o0555)
    except (OSError, SSHException) as e:
        logging.error(
            "failed to upload %d bytes to file %s on host %s: %s",
            len(pre_receive_str),
            pre_receive_path,
            con.host,
            e,
        )
        return False
    return True


@task
def destroy_repo_scheduled(
    con: Connection,
    gitolite_path: str,
    issue_url: Optional[str] = None,
    delay: int = 30,
    gitweb_host: str = "vineale.torproject.org",
    gitolite_repo: str = "~/src/tor/gitolite-admin",
):
    """schedule repository destruction"""
    gitolite_path = gitolite_path.strip("/").removesuffix(".git")
    # XXX: hardcoded path
    repository_path = f"/srv/git.torproject.org/repositories/{gitolite_path}.git"
    logging.info(
        "preparing destroying of Gitolite repository %s in %s",
        gitolite_path,
        repository_path,
    )

    if delay < 1:
        raise Exit("invalid delay (less than one day): %s" % delay)

    when = (datetime.now() + timedelta(days=delay)).isoformat()

    # XXX: hardcoded URL
    pre_receive_str = f"""#!/bin/sh

cat <<EOF
WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! WARNING!

This repository is scheduled for destruction on {when}.

If that is unexpected or not desired, immediately reach out to us:

https://gitlab.torproject.org/tpo/tpa/team/-/wikis/support

EOF
"""
    if issue_url:
        pre_receive_str += f"""
See this issue for details:

    {issue_url}
"""

    pre_receive_str += """
WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! WARNING!
"""
    write_pre_receive_hook(con, gitolite_path, pre_receive_str)

    logging.info(
        "scheduling destruction of %s in %s days on %s",
        repository_path,
        delay,
        con.host,
    )
    schedule_delete(con, repository_path, f"{delay} days")

    gitweb = Connection(gitweb_host)
    # XXX: hardcoded path
    gitweb_repository_path = (
        f"/srv/gitweb.torproject.org/repositories/{gitolite_path}.git"
    )
    logging.info(
        "scheduling destruction of %s in %s days on %s",
        gitweb_repository_path,
        delay,
        gitweb.host,
    )
    schedule_delete(con, gitweb_repository_path, f"{delay} days")

    logging.info(
        'modifying gitolite.conf to add "config gitweb.category = Scheduled for destruction"'
    )
    commit_msg = f"""Repository {gitolite_path} scheduled for destruction"""

    if issue_url:
        commit_msg += f"""\n\nSee: {issue_url}"""
    if change_category(
        con,
        Path(gitolite_repo).expanduser() / "conf/gitolite.conf",
        gitolite_path,
        "Scheduled for destruction",
    ):
        commit_and_push(
            str(Path(gitolite_repo).expanduser()), "conf/gitolite.conf", commit_msg
        )


@task
def migrate_repo(
    con: Connection,
    gitolite_path: str,
    gitlab_path: str,
    issue_url: Optional[str] = None,
    import_project: bool = False,
    description: Optional[str] = None,
    name: Optional[str] = None,
    archive: bool = False,
    tor_puppet: str = "~/src/tor/tor-puppet/",
    gitolite_repo: str = "~/src/tor/gitolite-admin",
    commit: bool = True,
    create_missing_groups: bool = True,
    pre_receive_hook: bool = False,
):
    """migrate gitolite repo to gitlab project, con is gitolite server"""
    global CGIT_URL, GITLAB_URL
    gitolite_path = gitolite_path.strip("/").removesuffix(".git")
    gitlab_path = gitlab_path.strip("/").removesuffix(".git")
    gitlab_project = None
    if import_project:
        if name is None:
            name = gitlab_path.rsplit("/", maxsplit=1)[-1]
        logging.info("importing project into GitLab")
        # XXX: hardcoded URL
        import_url = "https://git.torproject.org/" + gitolite_path
        gitlab_project = create_project(
            con,
            path=gitlab_path,
            description=description,
            name=name,
            archive=archive,
            import_url=import_url,
            create_missing_groups=create_missing_groups,
        )

    logging.info(
        "migrating Gitolite repository %s%s.git to GitLab project %s%s",
        CGIT_URL,
        gitolite_path,
        GITLAB_URL,
        gitlab_path,
    )

    pre_receive_str = f"""#!/bin/sh

cat <<EOF
This repository has been migrated to GitLab:

    {GITLAB_URL}{gitlab_path}/

You can change your Git remote with:

    git remote set-url origin {GITLAB_URL}{gitlab_path}.git
"""
    if issue_url:
        pre_receive_str += f"""
See this issue for details:

    {issue_url}
"""
    pre_receive_str += """
EOF

exit 1"""
    if pre_receive_hook:
        write_pre_receive_hook(con, gitolite_path, pre_receive_str)

    tor_puppet = os.path.expanduser(tor_puppet)
    rewrite_map = os.path.join(
        tor_puppet, "modules/profile/files/git/gitolite2gitlab.txt"
    )
    commit_msg = f"""migrate Gitolite {gitolite_path} repository to GitLab"""

    if issue_url:
        commit_msg += f"""\n\nSee: {issue_url}"""
    logging.info("adding entry to rewrite_map %s", rewrite_map)
    try:
        with open(rewrite_map) as fp:
            lines = fp.readlines()
        lines.append(gitolite_path + " " + gitlab_path + "\n")
        with open(rewrite_map, "w") as fp:
            fp.write("".join(sorted(set(lines))))
    except OSError as e:
        logging.warning(
            "file %s not found in tor-puppet.git repository not found: %s",
            rewrite_map,
            e,
        )
        logging.info(
            'add this (without the quotes) to  tor-puppet.git: "%s %s"',
            gitolite_path,
            gitlab_path,
        )
    else:
        if commit:
            commit_and_push(tor_puppet, rewrite_map, commit_msg)

    logging.info(
        'modifying gitolite.conf to add: "config gitweb.category = Migrated to GitLab"',
    )
    if (
        change_category(
            con,
            Path(gitolite_repo).expanduser() / "conf/gitolite.conf",
            gitolite_path,
            "Migrated to GitLab",
        )
        and commit
    ):
        commit_and_push(
            str(Path(gitolite_repo).expanduser()), "conf/gitolite.conf", commit_msg
        )
    return gitlab_project


@task
def missing_repos(
    con: Connection,
    remote_repos_dir: str = "/srv/git.torproject.org/repositories/",
    local_conf_dir: str = "~/src/tor/gitolite-admin",
):
    """find repositories missing from gitolite.conf or the remote server"""
    conf_path = Path(local_conf_dir).expanduser() / "conf" / "gitolite.conf"
    local_repos_list = sorted(set(list_conf_repos_str(conf_path.open().read())))
    remote_repos_list = sorted(set(list_all_repos(con)))
    if local_repos_list == remote_repos_list:
        logging.info("%d repositories found on both, success", len(remote_repos_list))
    else:
        logging.warning(
            "%d repositories found on %s:%s, %d found in gitolite config %s",
            len(remote_repos_list),
            getattr(con, "host", "localhost"),
            remote_repos_dir,
            len(local_repos_list),
            conf_path,
        )
        missing_local = [x for x in local_repos_list if x not in remote_repos_list]
        missing_remote = [x for x in remote_repos_list if x not in local_repos_list]
        if missing_local:
            logging.info(
                "%d gitolite repos without actual repositories: %s",
                len(missing_local),
                " ".join(missing_local),
            )
        if missing_remote:
            logging.info(
                "%d remote repositories missing from gitolite: %s",
                len(missing_remote),
                " ".join(missing_remote),
            )


class UserForkNotFoundError(Exception):
    """gross hack to hijack control flow out of gitlab project selection"""


def pick_gitlab_project(gitolite_path: str, user_override: bool = True):
    """helper routine to guess a GitLab project matching the given gitolite repo"""
    global CGIT_URL
    name = gitolite_path.rsplit("/", maxsplit=1)[-1]
    gl_connector = GitLabConnector.singleton()
    gl_projects = {}
    gl_project_paths = []

    if user_override and gitolite_path.startswith("user/"):
        _, username, _ = gitolite_path.split("/", maxsplit=2)
        fork = username + "/" + name
        logging.info("user repository detected, trying to find fork %s", fork)
        try:
            p = gl_connector.gitlab.projects.get(fork, statistics=True)
        except gitlab.exceptions.GitlabError as e:
            raise UserForkNotFoundError("user has no fork on GitLab: %s" % e)
        else:
            gl_projects[p.path_with_namespace] = p
            gl_project_paths.append(p.path_with_namespace)

    if not gl_project_paths:
        logging.info("searching for projects matching %s", name)
        for p in gl_connector.gitlab.projects.list(
            search=name, iterator=True, statistics=True
        ):
            gl_projects[p.path_with_namespace] = p
            gl_project_paths.append(p.path_with_namespace)

    if not gl_project_paths:
        return None

    logging.info(
        "found %d GitLab projects matching '%s' (%s%s.git)",
        len(gl_project_paths),
        name,
        CGIT_URL,
        gitolite_path,
    )
    p = pick(sorted(gl_project_paths))
    if not p:
        return None
    return gl_projects[p]


@task
def mass_repos_migration(
    con: Connection,
    local_conf_dir: str = "~/src/tor/gitolite-admin",
    tor_puppet: str = "~/src/tor/tor-puppet/",
    noop: bool = False,
    confirm: bool = True,
    exclude: Optional[str] = None,
    include: Optional[str] = None,
):
    """massively migrate all gitolite repositories to GitLab"""
    project = None
    description = None
    category = None
    conf_path = Path(local_conf_dir).expanduser() / "conf" / "gitolite.conf"
    count = 0
    for line in Path(conf_path).expanduser().open().readlines():
        if m := re.match(r"^repo +(.*) *$", line):
            if re_include_exclude(project, include, exclude) and mass_migrate_one_repo(
                con, project, description, category, noop, confirm
            ):
                count += 1
            project = m.group(1)
            description = None
            category = None
            continue
        if m := re.match(r"^ +config +gitweb.category += +(.*)$", line):
            category = m.group(1)
            continue
        if m := re.match(r'[A-Za-z/-]+\s+"[^"]+"\s+=\s+"([^"]+)"\s*', line):
            description = m.group(1)
            continue

    if re_include_exclude(project, include, exclude) and mass_migrate_one_repo(
        con, project, description, category, noop, confirm
    ):
        count += 1

    if not noop:
        commit_msg = f"mass migration of {count} repositories"
        commit_and_push(
            str(Path(local_conf_dir).expanduser()), "conf/gitolite.conf", commit_msg
        )
        tor_puppet = os.path.expanduser(tor_puppet)
        rewrite_map = os.path.join(
            tor_puppet, "modules/profile/files/git/gitolite2gitlab.txt"
        )
        commit_and_push(tor_puppet, rewrite_map, commit_msg)


STATIC_FORKS_MAP = {
    "tpo": "tpo/web/tpo",
    "webwml": "tpo/web/webwml",
    "exitmap": "tpo/network-health/exitmap",
    "user-manual": "tpo/web/manual",
    "tor-browser-bundle": "legacy/gitolite/builders/tor-browser-bundle",
}
COMMON_FORKS_MAP: dict[str, str] = {}


@task
def mass_migrate_user_repo(
    con: Connection,
    gitolite_path: str,
    description: Optional[str],
    category: Optional[str],
    noop: bool = False,
    confirm: bool = True,
) -> bool:
    """special routine to migrate a user repository"""
    global COMMON_FORKS_MAP, CGIT_URL
    if not COMMON_FORKS_MAP:
        COMMON_FORKS_MAP = (
            dict(
                gitolite2gitlab_generator(
                    str(Path("~/src/tor/tor-puppet/").expanduser())
                )
            )
            | STATIC_FORKS_MAP
        )

    # XXX: hardcoded URL
    issue_url = "https://gitlab.torproject.org/tpo/tpa/team/-/issues/41215"
    assert gitolite_path.startswith("user/")
    name = gitolite_path.rsplit("/", maxsplit=1)[-1]
    _, username, _ = gitolite_path.split("/", maxsplit=2)

    gl_connector = GitLabConnector.singleton()
    if name in COMMON_FORKS_MAP.keys():
        parent_gitlab_path = COMMON_FORKS_MAP[name]
        logging.info("found existing gitolite %s map to %s", name, parent_gitlab_path)
    else:
        gl_projects = {}
        gl_project_paths = []
        for p in gl_connector.gitlab.projects.list(
            search=name, iterator=True, statistics=True
        ):
            gl_projects[p.path_with_namespace] = p
            gl_project_paths.append(p.path_with_namespace)
        if len(gl_project_paths) == 0:
            logging.warning("no matching gitlab project found for %s", gitolite_path)
            return False
        logging.info(
            "found %d GitLab projects matching '%s' (%s%s.git)",
            len(gl_project_paths),
            name,
            CGIT_URL,
            gitolite_path,
        )
        p = pick(
            sorted(gl_project_paths), "select parent to fork from, or enter to abort: "
        )
        if not p:
            logging.info("skipping repository %s", gitolite_path)
            return False
        parent_gitlab_path = p

    fork_namespace = "legacy/gitolite/user/" + username
    gitlab_name = parent_gitlab_path.rsplit("/", maxsplit=1)[-1]
    fork_path = fork_namespace + "/" + gitlab_name

    gitolite_repo_stats = is_empty_repo(
        con,
        # XXX: hardcoded path
        f"/srv/git.torproject.org/repositories/{gitolite_path}.git",
        sudo=True,
    )
    if not gitolite_repo_stats:
        if not yes_no(
            "redirect gitolite project %s into gitlab %s with desc '%s'"
            % (
                gitolite_path,
                parent_gitlab_path,
                description,
            )
        ):
            return False
        if noop:
            logging.info(
                "would redirect gitolite project %s into gitlab %s with desc '%s'",
                gitolite_path,
                parent_gitlab_path,
                description,
            )
            return False
        migrate_repo(
            con,
            gitolite_path=gitolite_path,
            gitlab_path=parent_gitlab_path,
            issue_url=issue_url,
            import_project=False,
            description=description,
            archive=True,
            commit=False,
        )
        return True

    if confirm and not yes_no(f"fork project {parent_gitlab_path} into {fork_path}"):
        logging.info("skipping repository %s", gitolite_path)
        return False
    if noop:
        logging.info(
            "would fork, force-push and migrate project %s into %s",
            gitolite_path,
            fork_path,
        )
        return False

    logging.info("loading project %s", parent_gitlab_path)
    parent_gitlab_project = gl_connector.gitlab.projects.get(parent_gitlab_path)
    gl_connector.get_group_or_create(fork_namespace)
    timer = Timer()
    logging.info(
        "forking project %s into namespace %s at %s",
        gitolite_path,
        fork_namespace,
        timer.stamp,
    )
    fork = parent_gitlab_project.forks.create({"namespace": fork_namespace})
    logging.info("waiting for fork to complete...")
    status = ""
    while True:
        try:
            p = gl_connector.gitlab.projects.get(fork_path)
        except gitlab.exceptions.GitlabError:
            logging.info("project doesn't exist yet, sleeping")
            time.sleep(1)
            continue
        if p.import_status and p.import_status == "finished":
            logging.info("fork finished")
            break
        if p.import_status != status:
            logging.info("fork status: %s, sleeping...", p.import_status)
            status = p.import_status
        time.sleep(1)
    if timer.diff().total_seconds() > 10:
        logging.info("completed: %s", timer)

    gitlab_path = fork.path_with_namespace
    if not getattr(con, "host", None):
        logging.info("building a new connect to gitolite server")
        # XXX: hardcoded hostname
        con = Connection("cupani.torproject.org")

    logging.info("cloning and force pushing from %s to %s", gitolite_path, gitlab_path)
    local_import(
        Context(),
        gitolite_path=gitolite_path,
        gitlab_path=gitlab_path,
        gitlab_project=None,  # have it reload, as the fork object is insufficient
        force=True,
    )
    logging.info("migrating repo")
    migrate_repo(
        con,
        gitolite_path=gitolite_path,
        gitlab_path=gitlab_path,
        issue_url=issue_url,
        import_project=False,
        description=description,
        archive=True,
        commit=False,
    )
    return True


def mass_migrate_one_repo(
    con: Connection,
    gitolite_path: Optional[str],
    description: Optional[str],
    category: Optional[str],
    noop: bool = False,
    confirm: bool = True,
) -> bool:
    """fully migrate one single repository"""
    import_project = True
    import_locally = False
    gitlab_project = None
    if not gitolite_path:
        return False
    if gitolite_path.startswith("@"):
        logging.warning("skipping meta repository %s", gitolite_path)
        return False
    if category and category.lower().strip() in (
        "scheduled for destruction",
        "migrated to gitlab",
    ):
        logging.info("skipping project %s in category %s", gitolite_path, category)
        return False
    logging.info(
        "processing project %s (%s) in category %s",
        gitolite_path,
        description,
        category,
    )
    # XXX: hardcoded URL
    issue_url = "https://gitlab.torproject.org/tpo/tpa/team/-/issues/41215"
    legacy_prefix = "legacy/gitolite"
    gitlab_path = os.path.join(legacy_prefix, gitolite_path)

    try:
        possible_gitlab_project = pick_gitlab_project(gitolite_path)
    except UserForkNotFoundError:
        logging.warning("no existing fork found, entering user fork subroutine")
        if mass_migrate_user_repo(
            con, gitolite_path, description, category, noop, confirm
        ):
            return True
        logging.info("user fork subroutine failed, resuming normal procedure")
        possible_gitlab_project = pick_gitlab_project(gitolite_path, False)
    # we found a match and user confirmed, let's see if it's a good match
    if possible_gitlab_project:
        gitlab_path = possible_gitlab_project.path_with_namespace
        gitlab_project = possible_gitlab_project
        if gitlab_project.statistics.get("commit_count"):
            logging.info("repository has commits, seems safe to skip import")
            import_project = False
        else:
            if not getattr(con, "host", None):
                # XXX: hardcoded hostname
                con = Connection("cupani.torproject.org")
            non_empty_stats = is_empty_repo(
                con,
                # XXX: hardcoded path
                f"/srv/git.torproject.org/repositories/{gitolite_path}.git",
                sudo=True,
            )
            if non_empty_stats:
                if not no_yes(
                    f"WARNING: GitLab {gitlab_path} empty but gitolite {gitolite_path} non-empty, really redirect??"
                ):
                    if yes_no("clone locally and push?"):
                        import_locally = True
                        import_project = False
                    else:
                        logging.warning("skipping project %s", gitolite_path)
                        return False
            else:
                logging.info("repository empty on both sides, safe to redirect")
                import_project = False

    if confirm and not yes_no(
        "%s gitolite project %s into gitlab %s%s?"
        % (
            "import" if import_project else "redirect",
            gitolite_path,
            gitlab_path,
            " with desc '%s'" % description if description else "",
        )
    ):
        logging.warning("skipping project %s as requested", gitolite_path)
        return False
    # XXX: hardcoded URL
    gitolite_url = f"https://git.torproject.org/{gitolite_path}"
    logging.info("checking if remote repo %s exists", gitolite_url)
    if (
        subprocess.run(
            ["git", "ls-remote", gitolite_url], stdout=subprocess.PIPE
        ).returncode
        != 0
    ):
        logging.warning("cannot find remote, falling back to local import")
        import_locally = True
        import_project = False
        if not gitlab_project:
            gitlab_project = create_project(
                con,
                path=gitlab_path,
                description=description,
                archive=True,
                create_missing_groups=True,
            )
    if noop:
        logging.info(
            "would %s gitolite project %s into gitlab %s with desc '%s'",
            "import" if import_project else "redirect",
            gitolite_path,
            gitlab_path,
            description,
        )
        return False
    logging.info(
        "%s gitolite project %s into gitlab %s with desc '%s'",
        "importing" if import_project else "redirecting",
        gitolite_path,
        gitlab_path,
        description,
    )
    if not getattr(con, "host", None):
        logging.info("building a new connect to cupani")
        # XXX: hardcoded hostname
        con = Connection("cupani.torproject.org")
    migrate_repo(
        con,
        gitolite_path=gitolite_path,
        gitlab_path=gitlab_path,
        issue_url=issue_url,
        import_project=import_project,
        description=description,
        archive=True,
        commit=False,
    )
    if import_locally:
        local_import(Context(), gitolite_path, gitlab_path, gitlab_project)
    return True


# XXX: hardcoded URLs
CGIT_URL = "https://gitweb.torproject.org/"
GITLAB_URL = "https://gitlab.torproject.org/"

URL_MAPS = {
    "/commit": "commits/HEAD",
    "": "",
    "/": "",
    "/summary": "",
    "/about": "",
    "/commit?id=0c5ea1b04c4725fccc5a98a25bede5c419e07fcd": "commit/0c5ea1b04c4725fccc5a98a25bede5c419e07fcd",
    "/diff?id=0c5ea1b04c4725fccc5a98a25bede5c419e07fcd": "commit/0c5ea1b04c4725fccc5a98a25bede5c419e07fcd",
    "/patch?id=0c5ea1b04c4725fccc5a98a25bede5c419e07fcd": "commit/0c5ea1b04c4725fccc5a98a25bede5c419e07fcd.patch",
    "/rawdiff?id=0c5ea1b04c4725fccc5a98a25bede5c419e07fcd": "commit/0c5ea1b04c4725fccc5a98a25bede5c419e07fcd.diff",
    "/log": "commits/HEAD",
    "/log?h=HEAD": "commits/HEAD",
    "/log?h=1.7": "commits/1.7",
    "/atom?h=HEAD": "commits/HEAD",
    "/atom?h=1.7": "commits/1.7",
    "/refs": "tags",
    "/tag?h=1.7": "tags/1.7",
    "/tree?id=HEAD": "tree/HEAD",
    "/tree/README?id=HEAD": "tree/HEAD/README",
    "/tree?id=1.7": "tree/1.7",
    "/tree/README?id=1.7": "tree/1.7/README",
    "/tree": "tree/HEAD",
    # "plain?h=HEAD": "raw/HEAD",  # makes no sense?
    "/plain/README": "raw/HEAD/README",
    "/plain/README?h=HEAD": "raw/HEAD/README",
    "/plain/README?h=1.7": "raw/1.7/README",
    # disabled, see
    # e.g. https://gitweb.torproject.org/tor.git/blame/.gitlab-ci.yml?h=tor-0.4.7.13
    # which says "Blame is disabled
    # "blame/foo?id=HEAD": "blame/HEAD/foo",
    # "blame/foo": "blame/HEAD/foo",
    "/stats": "graphs/HEAD",
}


@task
def redirection_tests(
    con: Connection,
    project_git: str = "flashproxy",
    project_gitlab: str = "tpo/anti-censorship/pluggable-transports/flashproxy",
    not_migrated: str = "anonbib",
    gitlab_access_token: Optional[str] = None,
):
    """test our gitolite to gitlab redirection endpoints work properly"""
    if not gitlab_access_token:
        gitlab_access_token = os.environ.get("GITLAB_PRIVATE_TOKEN", None)
    if gitlab_access_token:
        headers = {
            "PRIVATE-TOKEN": gitlab_access_token,
        }
    else:
        logging.warning(
            "missing API token, erroneous warnings may be raised for private repositories"
        )
        headers = {}

    # XXX: hardcoded URL
    base_gitlab_url = "https://git.torproject.org/" + project_git + ".git"
    logging.info("testing Gitolite remote %s", base_gitlab_url)
    env = os.environ | {"GIT_TERMINAL_PROMPT": "0"}
    ret = subprocess.run(
        ["git", "ls-remote", base_gitlab_url],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env=env,
    )
    if ret.returncode != 0:
        logging.warning(
            "could not run git ls-remote %s (exit code %s): %s",
            base_gitlab_url,
            ret.returncode,
            ret.stderr,
        )
    else:
        # stderr expects:
        # warning: redirecting to https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/flashproxy.git/
        if (
            ret.stderr.decode("utf-8").strip()
            != "warning: redirecting to " + GITLAB_URL + project_gitlab + ".git/"
        ):
            logging.warning(
                "could not find expected redirection to GitLab in stderr: %s",
                ret.stderr,
            )

    session = requests.Session()

    gitolite_url = CGIT_URL + not_migrated + ".git/"
    resp = session.get(gitolite_url, allow_redirects=False)
    if resp.status_code == 200:
        logging.info(
            "project %s correctly not redirecting in gitolite (%s)",
            not_migrated,
            gitolite_url,
        )
    else:
        logging.warning(
            "project %s unexpectedly redirecting in gitolite (%s -> %s)",
            not_migrated,
            gitolite_url,
            resp.headers.get("location"),
        )

    for url_git, url_gitlab in URL_MAPS.items():
        full_git_url = CGIT_URL + project_git + ".git" + url_git
        logging.info("checking redirection on %s", full_git_url)
        resp = session.head(full_git_url, allow_redirects=False)
        full_gitlab_url = GITLAB_URL + project_gitlab + "/"
        if len(url_gitlab) > 0:
            full_gitlab_url += "-/" + url_gitlab
        if resp.status_code != 302:
            logging.warning(
                "unexpected status code (%s), redirection not working on %s",
                resp.status_code,
                full_git_url,
            )
            continue
        if resp.headers["location"] != full_gitlab_url:
            logging.warning(
                "GitLab URL (%s) different than expected (%s)",
                resp.headers["location"],
                full_gitlab_url,
            )
            continue
        logging.info(
            "checking if GitLab side works on %s",
            full_gitlab_url,
        )
        resp = session.head(full_gitlab_url, allow_redirects=False, headers=headers)
        if resp.status_code == 302:
            if "users/sign_in" in resp.headers.get("location", []):
                logging.warning(
                    "cannot reach GitLab URL %s, status code: %s to users/sign_in, missing token?",
                    full_gitlab_url,
                    resp.status_code,
                )
                continue
        elif resp.status_code != 200:
            logging.warning(
                "cannot reach GitLab URL %s, status code: %s",
                full_gitlab_url,
                resp.status_code,
            )
            continue
        logging.info("URL %s correctly matched to %s", full_git_url, full_gitlab_url)


def gitolite2gitlab_generator(tor_puppet: str) -> Iterator[tuple[str, str]]:
    """helper function to run the gitolite to gitlab rewrite map into a dict"""
    tor_puppet = os.path.expanduser(tor_puppet)
    rewrite_map = os.path.join(
        tor_puppet, "modules/profile/files/git/gitolite2gitlab.txt"
    )
    with open(rewrite_map) as fp:
        for line in fp.readlines():
            if line.startswith("#"):
                continue
            line = line.strip()
            yield cast(tuple[str, str], tuple(line.split(maxsplit=1)))


@task
def check_empty_gitlab_repos(
    con: Connection,
    tor_puppet: str = "~/src/tor/tor-puppet/",
):
    """check migrated gitlab repos to find empty repositories

    This was done because we found out some GitLab projects were
    already present, but without a repository, and we're afraid we're
    redirecting into empty repositories, leading to data loss.
    """

    gl_connector = GitLabConnector()

    for gitolite_path, gitlab_path in gitolite2gitlab_generator(tor_puppet):
        try:
            gl_project = gl_connector.gitlab.projects.get(gitlab_path, statistics=True)
        except Exception as e:
            logging.error("gitlab failure on project %s: %s", gitlab_path, e)
            continue
        if not gl_project.statistics.get("commit_count"):
            if getattr(con, "host", None):
                non_empty_stats = is_empty_repo(
                    con,
                    # XXX: hardcoded path
                    f"/srv/git.torproject.org/repositories/{gitolite_path}.git",
                    sudo=True,
                )
                if non_empty_stats:
                    logging.warning(
                        "GitLab project %s has zero commits, but gitolite %s has commits: %s",
                        gitlab_path,
                        gitolite_path,
                        ", ".join(non_empty_stats),
                    )
                else:
                    logging.info(
                        "GitLab project %s empty, but Gitolite project %s too, ok",
                        gitlab_path,
                        gitolite_path,
                    )
            else:
                logging.warning("GitLab project %s has zero commits", gitlab_path)
        else:
            logging.info("GitLab project %s ok", gitlab_path)


@task
def local_import(
    con: Connection,
    gitolite_path: str,
    gitlab_path: str,
    uid: int = 2,  # hardcoded default (anarcat)
    gitlab_project=None,
    force: bool = False,
):
    """clone a repo locally and push back into GitLab, hacking perms as we need to"""
    global GITLAB_URL
    gl_connector = GitLabConnector.singleton()

    if not gitlab_project:
        gitlab_project = gl_connector.gitlab.projects.get(gitlab_path)
    try:
        pr = gitlab_project.pushrules.get()
        if pr:
            logging.info("deleting push rules: %s", pr)
            pr.delete()
    except gitlab.exceptions.GitlabError:
        pass
    try:
        for pb in gitlab_project.protectedbranches.list():
            logging.info("deleting branch protection: %s", pb)
            pb.delete()
    except gitlab.exceptions.GitlabError:
        pass

    # enable project repository if disabled
    #
    # this will not, hopefully, make a private repository public
    if gitlab_project.repository_access_level == "disabled":
        gitlab_project.repository_access_level = "enabled"
        gitlab_project.save()
    # unarchive repo, if archived
    archived = gitlab_project.archived
    if archived:
        gitlab_project.unarchive()
    # grant access to a user so we can push
    try:
        member = gitlab_project.members.create(
            {"user_id": str(uid), "access_level": gitlab.const.AccessLevel.OWNER}
        )
    except gitlab.exceptions.GitlabError:
        # *probably* already created
        member = None
    # clone repository locally and push back up
    with tempfile.TemporaryDirectory() as tmpdir:
        # XXX: hardcoded hostname and path
        gitolite_remote = f"git-rw.torproject.org:/srv/git.torproject.org/repositories/{gitolite_path}.git"
        local_path = os.path.join(tmpdir, gitolite_path)
        timer = Timer()
        logging.info(
            "cloning repository %s in %s at %s",
            gitolite_remote,
            local_path,
            timer.stamp,
        )
        con.run(f"git clone --mirror {gitolite_remote} {local_path}")
        if timer.diff().total_seconds() > 10:
            logging.info("completed: %s", timer)
        gitlab_remote = f"{GITLAB_URL}{gitlab_path}"
        con.run(f"git -C {local_path} remote add gitlab {gitlab_remote}")
        timer = Timer()
        logging.info("pushing to GitLab %s at %s", gitlab_remote, timer.stamp)
        force_str = "--force" if force else ""
        con.run(f"git -C {local_path} push {force_str} --all gitlab")
        if timer.diff().total_seconds() > 10:
            logging.info("completed: %s", timer)
    # reset repository to original state, unarchive and delete extra access
    if archived:
        gitlab_project.archive()
    if member:
        try:
            member.delete()
        except gitlab.exceptions.GitlabDeleteError:
            logging.warning("cannot delete member in project %s, ignoring", gitlab_path)


@task
def mass_migration_cleanup(con: Connection, group: str = "legacy/gitolite"):
    """archive and remove extra accesses from migrated repos"""
    gl_connector = GitLabConnector()
    projects = gl_connector.group_projects([group], archived=False)
    for p in projects:
        if not p.archived:
            logging.info("project %s not archived, archiving", p.path_with_namespace)
            p.archive()
            p.save()
        try:
            p.members.delete(2)
            logging.info(
                "deleted stray member 2 from project %s", p.path_with_namespace
            )
        except gitlab.exceptions.GitlabError:
            pass
        m = p.members.list()
        if m:
            logging.warning("members remaining: %s in %s", m, p.path_with_namespace)
